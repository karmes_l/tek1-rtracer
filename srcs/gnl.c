/*
** gnl.c for lib in /home/le_l/rendu/rework/gnl
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat Feb  7 11:23:57 2015 VIctor Le
** Last update Sat May 30 17:40:22 2015 VIctor Le
*/

#include <stdlib.h>
#include <unistd.h>
#include "gnl.h"
#include "error_managment.h"

/*
** Un strncat pour le get_next_line.
*/
static void	gnl_strncat(char *dest, const char *const src,
			    const int dest_size, const int end)
{
  int		i;
  int		j;

  if (dest != NULL && src != NULL)
    {
      i = dest_size;
      j = 0;
      while (i < end)
	dest[i++] = src[j++];
    }
  else
    puterror(GNL_ERR_STRNCAT);
}

/*
** buffer: tableau static.
** line: valeur NULL autorisée.
** i_line: réception d'adresse depuis gnl_core().
** Un realloc pour le get_next_line qui en plus ajoute la suite de la ligne.
** Retourne NULL sur erreur de malloc, la chaîne mallocquée sinon.
*/
static char	*gnl_realloc(const char *buffer, char *line,
			     int *i_line, const int add)
{
  char		*new_line;
  int		i;

  if ((new_line = malloc(sizeof(*new_line) * (*i_line + add + 1))) == NULL)
    {
      puterror(GNL_ERR_MALLOC);
      return (NULL);
    }
  i = 0;
  while (i <= *i_line + add)
    new_line[i++] = 0;
  if (line != NULL)
    {
      i = -1;
      while (++i < *i_line)
	new_line[i] = line[i];
      free(line);
    }
  gnl_strncat(new_line, buffer, *i_line, *i_line + add);
  *i_line += add;
  return (new_line);
}

/*
** i_buffer, line: réception d'adresse from gnl().
** buffer: tableau static
** Partie principale du buffer du get_next_line.
** Place les offsets à la bonne place.
** Retourne GNL_CONTINUE ou GNL_END (int).
*/
static int	gnl_core(char **line, const char *buffer,
			 const int char_read, int *i_buffer)
{
  static int	i_line;
  int		add;

  add = 0;
  while (add < char_read - *i_buffer && buffer[*i_buffer + add] != '\n')
    add += 1;
  if ((*line = gnl_realloc(buffer + *i_buffer, *line, &i_line, add)) == NULL)
    return (GNL_END);
  if (buffer[*i_buffer + add] == '\n')
    {
      *i_buffer = (*i_buffer + add + 1 >= char_read) ? 0 : *i_buffer + add + 1;
      i_line = 0;
      return (GNL_END);
    }
  *i_buffer = (*i_buffer + add + 1 >= char_read) ? 0 : *i_buffer + add + 1;
  return (GNL_CONTINUE);
}

/*
** Retourne une chaîne allouée, NULL sur erreur.
*/
char		*gnl(const int fd)
{
  char		*line;
  static char	buffer[GNL_BUFFER + 1];
  static int	char_read;
  static int	i_buffer;

  line = NULL;
  if (i_buffer > 0 && gnl_core(&line, buffer, char_read, &i_buffer) == GNL_END)
    return (line);
  while (i_buffer == 0)
    {
      if ((char_read = read(fd, buffer, GNL_BUFFER)) == -1)
	{
	  puterror(GNL_ERR_READ);
	  return (NULL);
	}
      else if (char_read == 0
	       || gnl_core(&line, buffer, char_read, &i_buffer) == GNL_END)
	return (line);
    }
  return (line);
}
