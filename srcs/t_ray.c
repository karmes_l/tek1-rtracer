/*
** t_ray.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 15:09:55 2015 Aurélien Metz
** Last update Fri Jun  5 19:44:21 2015 Aurélien Metz
*/

#include "t_ray.h"

void	t_ray_init(t_ray *const ray)
{
  t_coord_init(&ray->pos[0]);
  t_coord_init(&ray->pos[1]);
  ray->intensity = 1;
}
