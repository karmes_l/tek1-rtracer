/*
** raytracer.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon May 25 16:38:29 2015 Aurélien Metz
** Last update Sun Jun  7 20:51:28 2015 huy le
*/

#include "raytracer.h"

/*
** Initialise la droite de vision
** (de l'origin au pixel de l'image tourné)
*/
static void	init_vision(t_ray *const vision, const t_eye *const eye,
			    const int x, const int y)
{
  t_coord	px_image;

  t_ray_init(vision);
  px_image.x = D;
  px_image.y = (WIDTH / 2) - x;
  px_image.z = (HEIGHT / 2) - y;
  vision->pos[0].x = eye->pos.x;
  vision->pos[0].y = eye->pos.y;
  vision->pos[0].z = eye->pos.z;
  vision->pos[1].x = (px_image.x * eye->rotation[0].x
		      + px_image.y * eye->rotation[1].x
		      + px_image.z * eye->rotation[2].x);
  vision->pos[1].y = (px_image.x * eye->rotation[0].y
		      + px_image.y * eye->rotation[1].y
		      + px_image.z * eye->rotation[2].y);
  vision->pos[1].z = (px_image.x * eye->rotation[0].z
		      + px_image.y * eye->rotation[1].z
		      + px_image.z * eye->rotation[2].z);
}

/*
** Calcul les coordonées de l'impact d'un rayon sur un objet
*/
void		get_impact(t_coord *const impact, const t_ray *const ray,
			   const float dist)
{
  if (impact && ray)
    {
      impact->x = ray->pos[0].x + (ray->pos[1].x * dist);
      impact->y = ray->pos[0].y + (ray->pos[1].y * dist);
      impact->z = ray->pos[0].z + (ray->pos[1].z * dist);
    }
}

/*
** Affecte la distance de l'oeil à l'objet à chaque objet
** Et retourne un pointeur sur l'objet avec la plus petite distance
*/
t_obj		*search_shortest_dist(t_obj *list_obj, const t_ray *const ray,
				      const float min, const float max)
{
  t_obj		*shortest;

  shortest = list_obj;
  while (list_obj)
    {
      intersection(list_obj, ray, min, max);
      if ((list_obj->dist < shortest->dist &&
	   (list_obj->dist >= min && list_obj->dist < max))
	  || (shortest->dist < min
	      && (list_obj->dist >= min && list_obj->dist < max)))
	shortest = list_obj;
      list_obj = list_obj->next;
    }
  return (shortest);
}

/*
** Retourne la couleur en fonction d'un rayon passé en paramètre
*/
t_color		*send_ray(t_ray *const ray, t_scene *const scene)
{
  t_coord	impact;
  t_color	*color;
  t_obj		*obj;

  if ((color = malloc(sizeof(t_color))) == NULL)
    return (color);
  t_color_init(color);
  t_coord_init(&impact);
  obj = search_shortest_dist(scene->list_obj.head, ray, 1, 99999999999);
  if (obj == NULL || obj->dist < 1)
    return (color);
  get_impact(&impact, ray, obj->dist);
  if (obj->material.type != MT_BROAD_CAST)
    {
      free(color);
      return (specular_effect(ray, scene, obj, &impact));
    }
  ilumination(&impact, scene, ray, obj);
  ray->intensity *= (1 - obj->material.absorption / 100);
  t_color_cpy(color, &obj->color);
  t_color_compute(color, ray->intensity);
  return (color);
}

/*
** Détèrmine la couleure d'un pixel.
** Retourne une structure couleure correspondante.
** Retourne NULL en cas d'erreure (malloc).
*/
t_color	*raytracer(t_scene *const scene, const unsigned int x,
		   const unsigned int y)
{
  t_ray	vision;

  if (scene == NULL
      || scene->list_obj.head == NULL
      || scene->list_light.head == NULL)
    return (NULL);
  init_vision(&vision, &scene->eye, x, y);
  return (send_ray(&vision, scene));
}
