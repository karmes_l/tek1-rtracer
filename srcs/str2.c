/*
** str2.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 23:56:43 2015 VIctor Le
** Last update Tue May 26 22:32:31 2015 VIctor Le
*/

#include <unistd.h>

static int	my_strlen(const char *const s)
{
  int		i;

  if (!s)
    return (0);
  i = 0;
  while (s[i])
    ++i;
  return (i);
}

void	putnbr_warn(const int nb)
{
  int	divisor;
  char	digit;

  (void)write(2, "\033[33m", my_strlen("\033[33m"));
  divisor = 1;
  while (nb / divisor >= 10 || nb / divisor <= -10)
    divisor *= 10;
  if (nb < 0)
    (void)write(2, "-", 1);
  while (divisor > 0)
    {
      digit = (nb < 0) ? -(nb / divisor % 10) : nb / divisor % 10;
      digit += '0';
      (void)write(2, &digit, 1);
      divisor /= 10;
    }
  (void)write(2, "\033[0m", my_strlen("\033[0m"));
}

void	putnbr_err(const int nb)
{
  int	divisor;
  char	digit;

  (void)write(2, "\033[31m", my_strlen("\033[31m"));
  divisor = 1;
  while (nb / divisor >= 10 || nb / divisor <= -10)
    divisor *= 10;
  if (nb < 0)
    (void)write(2, "-", 1);
  while (divisor > 0)
    {
      digit = (nb < 0) ? -(nb / divisor % 10) : nb / divisor % 10;
      digit += '0';
      (void)write(2, &digit, 1);
      divisor /= 10;
    }
  (void)write(2, "\033[0m", my_strlen("\033[0m"));
}
