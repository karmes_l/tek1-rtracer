/*
** draw.c for rt in /home/mohade_m/rendu/MUL_2014_rtracer
** 
** Made by melvin mohadeb
** Login   <mohade_m@epitech.net>
** 
** Started on  Mon May 25 16:32:46 2015 melvin mohadeb
** Last update Sat Jun  6 13:25:55 2015 Aurélien Metz
*/

#include "draw.h"

static void	my_pixel_put_to_img(t_img *const img,
				    const t_color *const color,
				    const int x, const int y)
{
  unsigned int	addr;

  if ((img && color) && ((x >= 0 && x <= WIDTH) && (y >= 0 && y <= HEIGHT)))
    {
      addr = y * img->size_line + (img->bpp / 8) * x;
      img->data[addr + 2] = color->red;
      img->data[addr + 1] = color->green;
      img->data[addr + 0] = color->blue;
    }
}

void		draw(t_rt *const rt, t_scene *const scene)
{
  unsigned int	x;
  unsigned int	y;
  t_color	*color;

  x = 0;
  y = 0;
  while (y < HEIGHT)
    {
      color = raytracer(scene, x, y);
      my_pixel_put_to_img(&rt->img, color, x, y);
      if (x >= WIDTH)
	{
	  ++y;
	  x = 0;
	}
      ++x;
      free(color);
    }
}
