/*
** intersection.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 15:47:22 2015 Aurélien Metz
** Last update Sat Jun  6 16:48:13 2015 Aurélien Metz
*/

#include "intersection.h"

static void	init_tab(void (**const tab)(const t_ray *const, t_obj *const),
			 char *const type_tab)
{
  tab[0] = &sphere;
  type_tab[0] = OT_SPHERE;
  tab[1] = &cone;
  type_tab[1] = OT_CONE;
  tab[2] = &cylindre;
  type_tab[2] = OT_CYLINDER;
  tab[3] = &plan;
  type_tab[3] = OT_PLAN;
  tab[4] = NULL;
  type_tab[4] = 0;
}

void		intersection(t_obj *const obj, const t_ray *const ray)
{
  void		(*tab[NBR_OBJ + 1])(const t_ray *const, t_obj *const);
  char		type_tab[NBR_OBJ + 1];
  unsigned int	i;

  i = 0;
  init_tab(tab, type_tab);
  while (tab[i])
    {
      if (type_tab[i] == obj->type)
	tab[i](ray, obj);
      ++i;
    }
}
