/*
** fill_sphere.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed May 27 11:34:57 2015 VIctor Le
** Last update Wed Jun  3 22:34:45 2015 VIctor Le
*/

#include "parser.h"
#include "t_scene.h"
#include "fill_fct_tools.h"
#include "fill_obj_fct.h"
#include "field.h"

/*
** Tous ptrs non-vérifiés: fct static, usage unique, vérif in fill_sphere().
** Initialise des variables de fill_sphere().
*/
static void	fill_sphere_init_var(char **fields, t_obj_fct *fptr,
				     int *f_value)
{
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_MATERIAL;
  fields[2] = PARSER_FIELD_COLOR;
  fields[3] = PARSER_FIELD_RAYON;
  fields[4] = PARSER_FIELD_ABS;
  fptr[0] = &fill_obj_set_pos;
  fptr[1] = &fill_obj_set_material;
  fptr[2] = &fill_obj_set_color;
  fptr[3] = &fill_obj_set_rayon;
  fptr[4] = &fill_obj_set_abs;
  f_value[0] = F_POS;
  f_value[1] = F_MATERIAL;
  f_value[2] = F_COLOR;
  f_value[3] = F_RAYON;
  f_value[4] = F_ABS;
}

/*
** Tous trs non-vérifs: Vérif in fill_sphere().
** Deuxième partie de fill_sphere().
** Retourne PARSER_OK, PARSER_FAIL, PARSER_SKIP.
*/
static int	fill_sphere_2(t_scene *scene, char **wdtab, int *line,
			      int *field_filled)
{
  t_obj_fct	fptr[PARSER_SP_FIELDS];
  char		*fields[PARSER_SP_FIELDS];
  int		f_value[PARSER_SP_FIELDS];
  int		ret;
  int		i;

  fill_sphere_init_var(fields, fptr, f_value);
  if ((i = get_index(PARSER_SP_FIELDS, fields, wdtab[0])) != PARSER_NO_MATCH)
    {
      ret = fptr[i](scene->list_obj.tail, wdtab, line);
      *field_filled |= f_value[i];
    }
  else
    ret = PARSER_SKIP;
  return (ret);
}

/*
** Tous ptrs non-vérifiés: vérification in parse_line().
** Remplis une structure 't_obj' de 't_scene'.
** Retourne PARSER_OK, PARSER_WARN, PARSER_FAIL, PARSER_SKIP.
*/
int		fill_sphere(t_scene *scene, const int fd, int *line, char **s)
{
  char		**wdtab;
  int		ret;
  int		field_filled;

  if (!t_list_obj_append(&scene->list_obj))
    return (PARSER_FAIL);
  scene->list_obj.tail->type = OT_SPHERE;
  ret = PARSER_OK;
  field_filled = F_VOID;
  while (ret == PARSER_OK)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      ret = fill_sphere_2(scene, wdtab, line, &field_filled);
      if (ret != PARSER_SKIP)
	*s = secure_free(*s);
      wdtab = free_wdtab(wdtab);
    }
  if (field_filled == F_VOID || (NEC_FIELD_SP & field_filled) != NEC_FIELD_SP)
    {
      puterror(PARSER_ERR_FIELD(PARSER_KW_SPHERE));
      ret = PARSER_FAIL;
    }
  return (ret);
}
