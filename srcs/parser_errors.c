/*
** parser_errors.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 08:43:15 2015 VIctor Le
** Last update Wed Jun  3 22:50:42 2015 VIctor Le
*/

#include "parser_errors.h"

/*
** Affiche des erreurs sur pointeurs NULL.
** Retourne PARSER_FAIL (erreur).
*/
int	parser_err_param(t_scene *scene, const char *const filename)
{
  if (!scene)
    puterror(PARSER_ERR_SCENE);
  if (!filename)
    puterror(PARSER_ERR_FILENAME);
  return (PARSER_FAIL);
}

/*
** Message d'erreur sur fail de open.
** Retourne PARSER_FAIL (erreur).
*/
int	parser_err_open(const char *const filename)
{
  puterror(PARSER_ERR_OPEN);
  if (filename)
    puterror(filename);
  puterror("\n");
  return (PARSER_FAIL);
}

/*
** Affiche messages de warning ou d'erreur.
** Retourne PARSER_WARN ou PARSER_FAIL.
*/
int	parser_err_warn(const char *const msg, const int line, const int err)
{
  if (msg)
    {
      if (err == PARSER_FAIL)
	{
	  puterror(msg);
	  putnbr_err(line);
	  puterror("\n");
	}
      else if (err == PARSER_WARN)
	{
	  putwarn(msg);
	  putnbr_warn(line);
	  putwarn("\n");
	}
    }
  return (err);
}

/*
** Affiche des erreurs sur pointeurs NULL pour get_index().
** Retourne PARSER_NO_MATCH.
*/
int	get_index_err_param(const char *const word, char **cmp_word)
{
  if (!word)
    puterror(GET_INDEX_ERR_WORD);
  if (!cmp_word)
    puterror(GET_INDEX_ERR_CMP_WORD);
  return (PARSER_NO_MATCH);
}
