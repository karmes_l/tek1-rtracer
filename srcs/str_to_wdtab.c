/*
** str_to_wdtab.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 22:21:54 2015 VIctor Le
** Last update Sat May 30 17:50:09 2015 VIctor Le
*/

#include <stdlib.h>
#include "error_managment.h"
#include "free_wdtab.h"
#include "str_to_wdtab.h"

/*
** delim: vérif in str_to_wdtab().
** Vérifie que la lettre est un délimiteur (\0 est exclu).
** Retourne STW_OK si c'est le cas, STW_FAIL sinon.
*/
static int	is_in_delim(const char c, const char *const delim)
{
  int		i;

  if (!c)
    return (STW_FAIL);
  i = 0;
  while (delim[i] && c != delim[i])
    ++i;
  if (c == delim[i])
    return (STW_OK);
  return (STW_FAIL);
}

/*
** all ptrs: vérif in str_to_wdtab.
** s et delim non-vérifiés: fonction à usage unique && vérifié en amont.
** Retourne le nombre de mot découpé par les délimiteurs.
*/
static int	get_nb_words(const char *s, const char *const delim)
{
  int		nb_words;

  while (*s && is_in_delim(*s, delim) == STW_OK)
    ++s;
  nb_words = 0;
  while (*s)
    {
      while (*s && is_in_delim(*s, delim) != STW_OK)
	++s;
      while (*s && is_in_delim(*s, delim) == STW_OK)
	++s;
      ++nb_words;
    }
  return (nb_words);
}

/*
** all ptrs: vérif in str_to_wdtab().
** Malloc la taille nécessaire pour chaque mot.
** Retourne STW_OK, STW_FAIL sur erreur.
*/
static int	malloc_words(char **wdtab, const char *s,
			     const char *const delim)
{
  int		i;
  int		len;

  while (*s && is_in_delim(*s, delim) == STW_OK)
    ++s;
  i = 0;
  while (*s)
    {
      len = 0;
      while (*s && is_in_delim(*s, delim) != STW_OK)
	{
	  ++len;
	  ++s;
	}
      while (*s && is_in_delim(*s, delim) == STW_OK)
	++s;
      if ((wdtab[i] = malloc(sizeof(**wdtab) * (len + 1))) == NULL)
	{
	  wdtab = free_wdtab(wdtab);
	  puterror(STW_ERR_MALLOC);
	  return (STW_FAIL);
	}
      ++i;
    }
  return (STW_OK);
}

/*
** all ptrs: vérif in str_to_wdtab().
** Remplis les chaînes du tableau.
*/
static void	fill_words(char **wdtab, const char *s,
			   const char *const delim)
{
  int		i;
  int		j;

  while (*s && is_in_delim(*s, delim) == STW_OK)
    ++s;
  i = 0;
  while (*s)
    {
      j = 0;
      while (*s && is_in_delim(*s, delim) != STW_OK)
	{
	  wdtab[i][j] = *s;
	  ++j;
	  ++s;
	}
      wdtab[i][j] = '\0';
      while (*s && is_in_delim(*s, delim) == STW_OK)
	++s;
      ++i;
    }
}

/*
** Retourne un tableau de mots terminé par NULL, NULL sur erreur.
*/
char	**str_to_wdtab(const char *s, const char *const delim)
{
  char	**wdtab;
  int	size;

  if (!s || !delim)
    {
      puterror(STW_ERR_NULL_PARAM);
      return (NULL);
    }
  size = get_nb_words(s, delim);
  if (!(wdtab = malloc(sizeof(*wdtab) * (size + 1))))
    {
      puterror(STW_ERR_MALLOC);
      return (NULL);
    }
  wdtab[size] = NULL;
  if (malloc_words(wdtab, s, delim) == STW_FAIL)
    return (NULL);
  fill_words(wdtab, s, delim);
  return (wdtab);
}
