/*
** light.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu May 28 14:27:08 2015 Aurélien Metz
** Last update Sun Jun  7 00:13:17 2015 Aurélien Metz
*/

#include "light_managment.h"

/*
** Calcul la contribution d'un spot lumineux apliqué à l'impacte.
*/
static float	spot(const float intensity, const t_coord *const normal,
		     const t_coord *const ray, const t_coord *const inc_ray)
{
  return (intensity
	  * MAX(0, scalaire(ray, normal) / (norm(normal) * norm(ray)))
	  * MAX(0, -scalaire(inc_ray, normal) / (norm(normal) * norm(inc_ray)))
	  );
}

/*
** Calcul l'intenstité de la lumière apliqué à l'impacte.
*/
static float	direct_ilumination(t_scene *const scene,
				   const t_obj *const obj,
				   const t_coord *const impact,
				   const t_ray *const inc_ray)
{
  float		intensity;
  t_coord	normal;
  t_light	*list_light;
  t_obj		*obstruct;
  t_ray		ray;

  intensity = 0;
  list_light = scene->list_light.head;
  t_coord_cpy(ray.pos, impact);
  while (list_light)
    {
      ray.pos[1].x = list_light->pos.x - impact->x;
      ray.pos[1].y = list_light->pos.y - impact->y;
      ray.pos[1].z = list_light->pos.z - impact->z;
      obstruct = search_shortest_dist(scene->list_obj.head, &ray, 0.01, 1);
      if (obstruct->dist <= 0.01 || obstruct->dist > 1)
	{
	  get_normal(&normal, obj, impact);
	  intensity += spot(list_light->intensity, &normal,
			    &ray.pos[1], &inc_ray->pos[1]);
	}
      list_light = list_light->next;
    }
  return (intensity);
}

void	ilumination(const t_coord *const impact, t_scene *const scene,
		    t_ray *const ray, const t_obj *const obj)
{
  float	intensity;

  intensity = direct_ilumination(scene, obj, impact, ray);
  if (intensity == 0)
    intensity = 0;
  intensity /= CURS;
  ray->intensity *= intensity;
}
