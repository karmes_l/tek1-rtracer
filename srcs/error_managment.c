/*
** error_managment.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 16:06:58 2015 Aurélien Metz
** Last update Wed Jun  3 18:33:12 2015 Aurélien Metz
*/

#include "error_managment.h"

int	error_managment(const int ac, const char *const *const env)
{
  if (ac != 2)
    {
      puterror(USAGE);
      return (0);
    }
  if (env == NULL || env[0] == NULL)
    {
      puterror(NULL_ENV);
      return (0);
    }
  return (1);
}

/*
** Vérifie que l'adresse reçue n'est pas NULL.
** Sinon affiche le message donné.
** Retourne l'adresse elle-même.
*/
void	*vgetaddr(void *const addr, const char *const msg_err)
{
  if (addr == NULL)
    puterror(msg_err);
  return (addr);
}
