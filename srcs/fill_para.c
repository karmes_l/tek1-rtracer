/*
** fill_para.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon Jun  1 17:39:40 2015 VIctor Le
** Last update Wed Jun  3 22:37:12 2015 VIctor Le
*/

#include "parser.h"
#include "fill_obj_fct.h"
#include "field.h"
#include "t_scene.h"

/*
** Tous ptrs non-vérifs: Vérif in fill_para().
** Initialise les variables de fill_para_2().
*/
static void	fill_para_init_var(t_obj_fct *fptr, char **fields, int *f_value)
{
  fptr[0] = &fill_obj_set_pos;
  fptr[1] = &fill_obj_set_bi_rotation;
  fptr[2] = &fill_obj_set_material;
  fptr[3] = &fill_obj_set_abs;
  fptr[4] = &fill_obj_set_color;
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_ROTATION;
  fields[2] = PARSER_FIELD_MATERIAL;
  fields[3] = PARSER_FIELD_ABS;
  fields[4] = PARSER_FIELD_COLOR;
  f_value[0] = F_POS;
  f_value[1] = F_ROTATION;
  f_value[2] = F_MATERIAL;
  f_value[3] = F_ABS;
  f_value[4] = F_COLOR;
}

/*
** Tous trs non-vérifs: Vérif in fill_para().
** Deuxième partie de fill_sphere().
** Retourne PARSER_OK, PARSER_FAIL, PARSER_SKIP.
*/
static int	fill_para_2(t_scene *scene, char **wdtab, int *line,
			    int *field_filled)
{
  t_obj_fct	fptr[PARSER_PA_FIELDS];
  char		*fields[PARSER_PA_FIELDS];
  int		f_value[PARSER_PA_FIELDS];
  int		ret;
  int		i;

  fill_para_init_var(fptr, fields, f_value);
  if ((i = get_index(PARSER_PA_FIELDS, fields, wdtab[0])) != PARSER_NO_MATCH)
    {
      ret = fptr[i](scene->list_obj.tail, wdtab, line);
      *field_filled |= f_value[i];
    }
  else
    ret = PARSER_SKIP;
  return (ret);
}

/*
** Tous ptrs non-vérifiés: vérification in parse_line().
** Remplis une structure 't_obj' de 't_scene'.
** Retourne PARSER_OK, PARSER_WARN, PARSER_FAIL, PARSER_SKIP.
*/
int	fill_para(t_scene *scene, const int fd, int *line, char **s)
{
  char	**wdtab;
  int	ret;
  int	field_filled;

  if (!t_list_obj_append(&scene->list_obj))
    return (PARSER_FAIL);
  scene->list_obj.tail->type = OT_PARA;
  ret = PARSER_OK;
  field_filled = F_VOID;
  while (ret == PARSER_OK)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      ret = fill_para_2(scene, wdtab, line, &field_filled);
      if (ret != PARSER_SKIP)
	*s = secure_free(*s);
      wdtab = free_wdtab(wdtab);
    }
  if (field_filled == F_VOID || (NEC_FIELD_PA & field_filled) != NEC_FIELD_PA)
    {
      puterror(PARSER_ERR_FIELD(PARSER_KW_PARA));
      ret = PARSER_FAIL;
    }
  return (ret);
}
