/*
** event.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May 24 15:52:17 2015 Aurélien Metz
** Last update Sun May 31 17:59:13 2015 Aurélien Metz
*/

#include "event.h"

int	expose(t_rt *rt)
{
  mlx_put_image_to_window(rt->mlx_ptr, rt->win_ptr, rt->img.img_ptr, 0, 0);
  return (0);
}

int	key(const int keycode, t_rt *const rt)
{
  if (keycode == ESC)
    {
      mlx_destroy_window(rt->mlx_ptr, rt->win_ptr);
      t_scene_destruct(rt->scene);
      exit(0);
    }
  return (0);
}
