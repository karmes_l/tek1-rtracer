/*
** cone.c for cone in /home/le_n/rendu/igraph/MUL_2014_rtracer/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Thu May 28 13:39:40 2015 huy le
** Last update Sun Jun  7 20:52:59 2015 huy le
*/

#include "cone.h"

static void	init_cone(t_coord *const v_co_eye, const t_ray *const ray,
			  const t_obj *const obj, float *const equation)
{
  v_co_eye->x = ray->pos[0].x - obj->pos.x;
  v_co_eye->y = ray->pos[0].y - obj->pos.y;
  v_co_eye->z = ray->pos[0].z - obj->pos.z;
  equation[0] = P2(scalaire(&obj->rotation[0], &ray->pos[1]))
    - (P2(norm(&obj->rotation[0]))
       * P2(norm(&ray->pos[1]))
       * P2(cos(CONVERT(obj->angle))));
  equation[1] = (scalaire(&obj->rotation[0], v_co_eye)
		 * scalaire(&obj->rotation[0], &ray->pos[1])
		 - (scalaire(v_co_eye, &ray->pos[1])
		    * P2(norm(&obj->rotation[0]))
		    * P2(cos(CONVERT(obj->angle))))) * 2;
  equation[2] = P2(scalaire(&obj->rotation[0], v_co_eye)) - P2(norm(v_co_eye))
    * P2(norm(&obj->rotation[0])) * P2(cos(CONVERT(obj->angle)));
}

static int	limit_cone(const t_ray *const ray, const t_obj *const obj)
{
  t_coord	impact;
  t_coord	v_co_impact;

  get_impact(&impact, ray, obj->dist);
  v_co_impact.x = impact.x - obj->pos.x;
  v_co_impact.y = impact.y - obj->pos.y;
  v_co_impact.z = impact.z - obj->pos.z;
  if ((scalaire(&obj->rotation[0], &v_co_impact) >= 0
       && scalaire(&obj->rotation[0], &v_co_impact) / norm(&obj->rotation[0])
       <= obj->limit[0])
      || (scalaire(&obj->rotation[0], &v_co_impact) <= 0
	  && scalaire(&obj->rotation[0], &v_co_impact) / norm(&obj->rotation[0])
	  <= obj->limit[1]))
    return (1);
  return (0);
}

/*
** ray et obj non checké : verification des ptr en amont
*/
void		cone(const t_ray *const ray, t_obj *const obj)
{
  float		delta;
  float		equation[3];
  float		dist[2];
  t_coord	v_co_eye;

  obj->dist = 0;
  init_cone(&v_co_eye, ray, obj, equation);
  if ((delta = DELTA(equation[0], equation[1], equation[2])) >= 0)
    {
      dist[0] = (-equation[1] - sqrt(delta)) / (2 * equation[0]);
      dist[1] = (-equation[1] + sqrt(delta)) / (2 * equation[0]);
      obj->dist = MIN_POS(dist[0], dist[1]);
      if ((obj->limit[0] || obj->limit[1]) && limit_cone(ray, obj) == 0)
	obj->dist = 0;
    }
}
