/*
** str.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 16:09:18 2015 Aurélien Metz
** Last update Tue May 26 22:33:18 2015 VIctor Le
*/

#include <unistd.h>

void	my_putchar(const char c)
{
  (void)write(1, &c, 1);
}

int		my_strlen(const char *const s)
{
  unsigned int	i;

  if (!s)
    return (0);
  i = 0;
  while (s[i])
    ++i;
  return (i);
}

void	putstr(const char *const s)
{
  if (s)
    (void)write(1, s, my_strlen(s));
}

void	puterror(const char *const s)
{
  if (s)
    {
      (void)write(2, "\033[31m", my_strlen("\033[31m"));
      (void)write(2, s, my_strlen(s));
      (void)write(2, "\033[0m", my_strlen("\033[0m"));
    }
}

void	putwarn(const char *const s)
{
  if (s)
    {
      (void)write(2, "\033[33m", my_strlen("\033[33m"));
      (void)write(2, s, my_strlen(s));
      (void)write(2, "\033[0m", my_strlen("\033[0m"));
    }
}
