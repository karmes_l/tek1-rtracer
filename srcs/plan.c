/*
** plan.c for plan in /home/le_n/rendu/igraph/MUL_2014_rtracer/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Thu May 28 14:40:32 2015 huy le
** Last update Sat Jun  6 17:55:25 2015 Aurélien Metz
*/

#include "plan.h"

/*
** ray et obj non checké : verification des ptr en amont
*/
void		plan(const t_ray *const ray, t_obj *const obj)
{
  t_coord	normal;
  t_coord	v_p_eye;

  v_p_eye.x = ray->pos->x - obj->pos.x;
  v_p_eye.y = ray->pos->y - obj->pos.y;
  v_p_eye.z = ray->pos->z - obj->pos.z;
  pvector(&obj->rotation[0], &obj->rotation[1], &normal);
  if (scalaire(&ray->pos[1], &normal) == 0)
    obj->dist = 0;
  obj->dist = -(scalaire(&v_p_eye, &normal) / scalaire(&ray->pos[1], &normal));
}
