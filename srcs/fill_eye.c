/*
** fill_eye.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 16:15:49 2015 VIctor Le
** Last update Wed Jun  3 22:43:53 2015 VIctor Le
*/

#include "parser.h"
#include "fill_fct_tools.h"

/*
** Tous ptrs non-vérifiés: vérification en amont in fill_eye().
** Remplis une structure 't_coord' de 't_eye'.
** Retourne PARSER_OK, PARSER_FAIL.
*/
static int	fill_eye_coord(t_coord *coord, char **wdtab, int *line)
{
  int		i;
  float		*tmp[3];

  if (check_nb_field_args(PARSER_NB_ARG_COORD, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  tmp[0] = &coord->x;
  tmp[1] = &coord->y;
  tmp[2] = &coord->z;
  i = 1;
  while (i < PARSER_NB_ARG_COORD)
    {
      (*tmp)[i - 1] = atof(wdtab[i]);
      ++i;
    }
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifiés: fct static, usage unique, vérif in fill_eye().
** Initialise les tableaux de la fonction fill_eye_var_init().
*/
static void	fill_eye_var_init(char **fields, t_coord **tmp, t_scene *scene)
{
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_LOOK;
  tmp[0] = &scene->eye.pos;
  tmp[1] = &scene->eye.look;
}

/*
** Tous ptrs non-vérifiés: vérification in parse_line().
** Remplis la structure 't_eye' de 't_scene'.
** Retourne PARSER_OK, PARSER_WARN ou PARSER_FAIL.
*/
int		fill_eye(t_scene *scene, const int fd, int *line, char **s)
{
  t_coord	*tmp[PARSER_EYE_FIELDS];
  char		*fields[PARSER_EYE_FIELDS];
  char		**wdtab;
  int		i;
  int		ret;

  fill_eye_var_init(fields, tmp, scene);
  i = 0;
  ret = PARSER_OK;
  while (i != PARSER_NO_MATCH && ret != PARSER_FAIL)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      if ((i = get_index(PARSER_EYE_FIELDS, fields, wdtab[0]))
	  != PARSER_NO_MATCH)
	{
	  free(*s);
	  ret = fill_eye_coord(tmp[i], wdtab, line);
	}
      else
	ret = PARSER_SKIP;
      wdtab = free_wdtab(wdtab);
    }
  return (ret);
}
