/*
** parser.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:56:07 2015 VIctor Le
** Last update Sun Jun  7 16:15:42 2015 VIctor Le
*/

#include "parser.h"

/*
** keywords non-vérifié: fct static, usage unique, initialise.
** Remplis le tableau avec les keywords du parser.
*/
static void	fill_keywords_and_t_fill(char **keywords, t_fill *fptr)
{
  keywords[0] = PARSER_KW_EYE;
  keywords[1] = PARSER_KW_LIGHT;
  keywords[2] = PARSER_KW_SPHERE;
  keywords[3] = PARSER_KW_CONE;
  keywords[4] = PARSER_KW_CYLINDER;
  keywords[5] = PARSER_KW_PLAN;
  keywords[6] = PARSER_KW_TRIANGLE;
  keywords[7] = PARSER_KW_PARA;
  keywords[8] = PARSER_KW_DISC;
  fptr[0] = &fill_eye;
  fptr[1] = &fill_light;
  fptr[2] = &fill_sphere;
  fptr[3] = &fill_cone;
  fptr[4] = &fill_cylinder;
  fptr[5] = &fill_plan;
  fptr[6] = &fill_triangle;
  fptr[7] = &fill_para;
  fptr[8] = &fill_disc;
}

/*
** wdtab et s non-vérifié: vérif in parser().
** line non-vérifié: réception d'adresse depuis parser().
** Parse la ligne reçue via gnl.
** Retourne PARSER_OK, PARSER_WARN, PARSER_FAIL, PARSER_SKIP.
*/
static int	parse_line(t_scene *scene, char **s, int *line, const int fd)
{
  int		i;
  char		**wdtab;
  char		*keywords[PARSER_NB_KW];
  t_fill	fptr[PARSER_NB_KW];

  fill_keywords_and_t_fill(keywords, fptr);
  if (!(wdtab = str_to_wdtab(*s, " \t")))
    {
      *s = secure_free(*s);
      return (PARSER_FAIL);
    }
  free(*s);
  if ((i = get_index(PARSER_NB_KW, keywords, wdtab[0])) == PARSER_NO_MATCH)
    {
      print_no_match(wdtab[0], *line);
      wdtab = free_wdtab(wdtab);
      *s = get_next_non_empty_line(fd, line);
      return (PARSER_SKIP);
    }
  wdtab = free_wdtab(wdtab);
  return (fptr[i](scene, fd, line, s));
}

/*
** Parser de fichier de notre ray-tracer.
** Retourne PARSER_FAIL sur erreur, 1 sinon.
*/
int	parser(t_scene *scene, const char *const filename)
{
  int	fd;
  char	*s;
  int	line;
  int	ret;

  if (!scene || !filename)
    return (parser_err_param(scene, filename));
  if ((fd = open(filename, O_RDONLY)) == -1)
    return (parser_err_open(filename));  line = 0;
  s = get_next_non_empty_line(fd, &line);
  ret = PARSER_OK;
  while (s && ret != PARSER_END)
    {
      if ((ret = parse_line(scene, &s, &line, fd)) == PARSER_FAIL)
	{
	  s = secure_free(s);
	  close(fd);
	  return (PARSER_FAIL);
	}
    }
  s = secure_free(s);
  close(fd);
  return (PARSER_OK);
}
