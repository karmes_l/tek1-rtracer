/*
** get_normal.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 15:20:35 2015 Aurélien Metz
** Last update Sun Jun  7 20:53:41 2015 huy le
*/

#include "get_normal.h"

static void	init_normal_obj_tab(t_normal_obj *const normal_obj,
				    char *const type_tab)
{
  normal_obj[0] = norm_vect_sphere;
  type_tab[0] = OT_SPHERE;
  normal_obj[1] = norm_vect_cone;
  type_tab[1] = OT_CONE;
  normal_obj[2] = norm_vect_cy;
  type_tab[2] = OT_CYLINDER;
  normal_obj[3] = norm_vect_plan;
  type_tab[3] = OT_PLAN;
  normal_obj[4] = NULL;
  type_tab[4] = 0;
}

void		get_normal(t_coord *const normal, const t_obj *const obj,
			   const t_coord *const impact)
{
  t_normal_obj	normal_obj[NBR_OBJ + 1];
  char		type_tab[NBR_OBJ + 1];
  unsigned int	i;

  i = 0;
  init_normal_obj_tab(normal_obj, type_tab);
  while (normal_obj[i])
    {
      if (type_tab[i] == obj->type)
	normal_obj[i](normal, obj, impact);
      ++i;
    }
}
