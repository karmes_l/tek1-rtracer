/*
** t_img.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 16:56:35 2015 Aurélien Metz
** Last update Sat May 30 21:07:17 2015 Aurélien Metz
*/

#include "t_img.h"

int	t_img_init(t_rt *const rt)
{
  if ((rt->img.img_ptr = vgetaddr(mlx_new_image(rt->mlx_ptr, WIDTH, HEIGHT),
				  MLX_NEW_IMAGE)) == NULL
      || (rt->img.data = vgetaddr(mlx_get_data_addr(rt->img.img_ptr,
						    &rt->img.bpp,
						    &rt->img.size_line,
						    &rt->img.endian),
				  MLX_GET_DATA_ADDR)) == NULL)
    return (0);
  return (1);
}
