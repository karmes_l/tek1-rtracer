/*
** cylindre.c for  in /home/le_n/rendu/igraph/MUL_2014_rtracer/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Thu May 28 14:16:43 2015 huy le
** Last update Sat Jun  6 22:41:19 2015 Aurélien Metz
*/

#include "cylindre.h"

static void	init_cylindre(t_coord *const v_cy_eye,
			      const t_ray *const ray, const t_obj *const obj)
{
  v_cy_eye->x = ray->pos[0].x - obj->pos.x;
  v_cy_eye->y = ray->pos[0].y - obj->pos.y;
  v_cy_eye->z = ray->pos[0].z - obj->pos.z;
}

static int	limit_cylindre(const t_ray *const ray, t_obj *const obj)
{
  t_coord	impact;
  t_coord	v_cy_impact;

  get_impact(&impact, ray, obj->dist);
  v_cy_impact.x = impact.x - obj->pos.x;
  v_cy_impact.y = impact.y - obj->pos.y;
  v_cy_impact.z = impact.z - obj->pos.z;
  if ((scalaire(obj->rotation, &v_cy_impact) / norm(obj->rotation))
      <= obj->limit[0] / 2)
    return (1);
  return (0);
}

/*
** ray et obj non checké : verification des ptr en amont
*/
void		cylindre(const t_ray *const ray, t_obj *obj)
{
  t_coord	v_cy_eye;
  t_coord	pv_vcy_eye_vcy;
  t_coord	pv_vray_vcy;
  float		delta;
  float		equation[3];
  float		dist[2];

  obj->dist = 0;
  init_cylindre(&v_cy_eye, ray, obj);
  pvector(&ray->pos[1], obj->rotation, &pv_vray_vcy);
  pvector(&v_cy_eye, obj->rotation, &pv_vcy_eye_vcy);
  equation[0] = P2(norm(&pv_vray_vcy));
  equation[1] = 2 * scalaire(&pv_vray_vcy, &pv_vcy_eye_vcy);
  equation[2] = P2(norm(&pv_vcy_eye_vcy))
    - P2(obj->rayon) * P2(norm(obj->rotation));
  if ((delta = DELTA(equation[0], equation[1], equation[2])) >= 0)
    {
      dist[0] = (-equation[1] - sqrt(delta)) / (2 * equation[0]);
      dist[1] = (-equation[1] + sqrt(delta)) / (2 * equation[0]);
      obj->dist = MIN_POS(dist[0], dist[1]);
      if (obj->limit[0] != 0 && limit_cylindre(ray, obj) == 0)
	obj->dist = 0;
    }
}
