/*
** calcul.c for  in /home/le_n/rendu/igraph/MUL_2014_rtracer/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Thu May 28 14:37:08 2015 huy le
** Last update Thu Jun  4 18:34:57 2015 Aurélien Metz
*/

#include <math.h>
#include "coord.h"

void	pvector(const t_coord *const v1, const t_coord *const v2,
		t_coord *const pv)
{
  pv->x = v1->y * v2->z - v2->y * v1->z;
  pv->y = v1->z * v2->x - v2->z * v1->x;
  pv->z = v1->x * v2->y - v2->x * v1->y;
}

float	scalaire(const t_coord *const u, const t_coord *const v)
{
  return (u->x * v->x + u->y * v->y + u->z * v->z);
}

float	norm(const t_coord *const u)
{
  return (sqrt((u->x * u->x)
	       + (u->y * u->y)
	       + (u->z * u->z)));
}

float		dist_seg(t_coord *pt1, t_coord *pt2)
{
  t_coord	seg;

  seg.x = pt2->x - pt1->x;
  seg.y = pt2->y - pt1->y;
  seg.z = pt2->z - pt1->z;
  return (norm(&seg));
}
