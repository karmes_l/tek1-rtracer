/*
** fill_light.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 16:17:18 2015 VIctor Le
** Last update Thu May 28 17:48:38 2015 VIctor Le
*/

#include "parser.h"
#include "t_scene.h"
#include "fill_fct_tools.h"
#include "fill_light_fct.h"

/*
** Tous ptrs non-vérifiés: fct static, usage unique, vérif in fill_light().
** Initialise certaines variables de fill_light().
*/
static void	fill_light_init_var(char **fields, t_light_fct *fptr,
				    int *i, int *ret)
{
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_INTST;
  fptr[0] = &fill_light_set_pos;
  fptr[1] = &fill_light_set_intensity;
  *i = 0;
  *ret = PARSER_OK;
}

/*
** Tous ptrs non-vérifiés: vérification in parse_line().
** Remplis la structure 't_light' de 't_scene'.
** Retourne PARSER_OK, PARSER_WARN, PARSER_FAIL.
*/
int		fill_light(t_scene *scene, const int fd, int *line, char **s)
{
  t_light_fct	fptr[PARSER_LIGHT_FIELDS];
  char		*fields[PARSER_LIGHT_FIELDS];
  char		**wdtab;
  int		i;
  int		ret;

  fill_light_init_var(fields, fptr, &i, &ret);
  if (!t_list_light_append(&scene->list_light))
    return (PARSER_FAIL);
  while (i != PARSER_NO_MATCH && ret != PARSER_FAIL)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      if ((i = get_index(PARSER_LIGHT_FIELDS, fields, wdtab[0]))
	  != PARSER_NO_MATCH)
	{
	  free(*s);
	  ret = fptr[i](scene->list_light.tail, wdtab, line);
	}
      else
	ret = PARSER_SKIP;
      wdtab = free_wdtab(wdtab);
    }
  return (ret);
}
