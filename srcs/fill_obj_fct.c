/*
** fill_obj_fct.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 28 16:51:03 2015 VIctor Le
** Last update Sun Jun  7 16:15:32 2015 VIctor Le
*/

#include "parser.h"
#include "fill_fct_tools.h"
#include "field.h"

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set la position d'un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_pos(t_obj *obj, char **wdtab, int *line)
{
  int	i;
  float	*tmp[3];

  if (check_nb_field_args(PARSER_NB_ARG_POS, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  tmp[0] = &obj->pos.x;
  tmp[1] = &obj->pos.y;
  tmp[2] = &obj->pos.z;
  i = 1;
  while (i < PARSER_NB_ARG_POS)
    {
      (*tmp)[i - 1] = atof(wdtab[i]);
      ++i;
    }
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set l'angle d'un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_angle(t_obj *obj, char **wdtab, int *line)
{
  float	tmp;

  if (check_nb_field_args(PARSER_NB_ARG_ANGLE, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  if ((tmp = atof(wdtab[1])) < 1)
    {
      puterror(PARSER_ERR_BAD_ANGLE);
      return (PARSER_FAIL);
    }
  obj->angle = (unsigned int)tmp;
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set un vecteur de rotation d'un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_rotation(t_obj *obj, char **wdtab, int *line)
{
  float	*tmp[3];
  int	i;

  if (check_nb_field_args(PARSER_NB_ARG_ROTATION, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  tmp[0] = &obj->rotation[0].x;
  tmp[1] = &obj->rotation[0].y;
  tmp[2] = &obj->rotation[0].z;
  i = 1;
  while (i < PARSER_NB_ARG_ROTATION)
    {
      (*tmp)[i - 1] = atof(wdtab[i]);
      ++i;
    }
  if (check_null_rotation(obj->rotation) == PARSER_FAIL)
    return (PARSER_FAIL);
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set le 'material' d'un objet.
** Retourne PARSER_OK, PARSER_FAIL
*/
int	fill_obj_set_material(t_obj *obj, char **wdtab, int *line)
{
  int	i;
  char	*field[MAT_NB_FIELD];
  char	f_value[MAT_NB_FIELD + 1];

  if (check_nb_field_args(PARSER_NB_ARG_MATERIAL, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  field[0] = MAT_FIELD_BC;
  field[1] = MAT_FIELD_TR;
  field[2] = MAT_FIELD_RE;
  f_value[0] = MT_BROAD_CAST;
  f_value[1] = MT_TRANSMIT;
  f_value[2] = MT_REFLECT;
  f_value[3] = MT_BROAD_CAST;
  i = 0;
  while (i < MAT_NB_FIELD && my_strcasecmp(field[i], wdtab[1]))
    ++i;
  if (obj->material.type != MT_VOID)
    {
      putwarn(PARSER_WARN_REDEF_MAT);
      putnbr_warn(*line);
      putwarn("\n");
    }
  obj->material.type = f_value[i];
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set 'color' d'un objet
** Retourne PARSER_OK, PARSER_FAIL.
*/
int		fill_obj_set_color(t_obj *obj, char **wdtab, int *line)
{
  unsigned char	*tmp[3];
  int		i;

  if (check_nb_field_args(PARSER_NB_ARG_COLOR, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  tmp[0] = &obj->color.red;
  tmp[1] = &obj->color.green;
  tmp[2] = &obj->color.blue;
  i = 1;
  while (i < PARSER_NB_ARG_COLOR)
    {
      (*tmp)[i - 1] = (unsigned char)atof(wdtab[i]);
      ++i;
    }
  return (PARSER_OK);
}
