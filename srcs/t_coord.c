/*
** t_coord.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:10:15 2015 VIctor Le
** Last update Thu May 28 23:25:03 2015 lionel karmes
*/

#include "coord.h"

void	t_coord_cpy(t_coord *const dest, const t_coord *const src)
{
  if (dest && src)
    {
      dest->x = src->x;
      dest->y = src->y;
      dest->z = src->z;
    }
}

void		t_coord_swap_coord(t_coord *coord1, t_coord *coord2)
{
  t_coord	tmp;

  if (coord1 && coord2)
    {
      tmp.x = coord1->x;
      tmp.y = coord1->y;
      tmp.z = coord1->z;
      t_coord_cpy(coord1, coord2);
      t_coord_cpy(coord2, &tmp);
    }
}

void	t_coord_init(t_coord *const coord)
{
  if (coord)
    {
      coord->x = 0;
      coord->y = 0;
      coord->z = 0;
    }
}
