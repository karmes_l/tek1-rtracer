/*
** t_material.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed May 27 12:03:32 2015 VIctor Le
** Last update Sat Jun  6 11:34:53 2015 Aurélien Metz
*/

#include "material.h"

void	t_material_init(t_material *const material)
{
  if (material)
    {
      material->type = MT_BROAD_CAST;
      material->absorption = 0;
    }
}
