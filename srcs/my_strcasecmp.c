/*
** my_strcasecmp.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 08:41:36 2015 VIctor Le
** Last update Sat Jun  6 16:10:47 2015 Aurélien Metz
*/

/*
** Check que 2 lettres sont les mêmes (casse ignorée).
** Retourne 1 si identique, PARSER_FAIL sinon.
*/
static int	is_same_letter(const char c1, const char c2)
{
  if (c1 == c2
      || ((c1 >= 'A' && c1 <= 'Z') && c2 == c1 + 32)
      || ((c2 >= 'A' && c2 <= 'Z') && c1 == c2 + 32))
    return (1);
  return (0);
}

/*
** Recode de la fonction strcasecmp.
** Retourne la diff entre les 2 premières lettres non-identiques,
**	    0 si l'une des chaînes est NULL.
*/
int	my_strcasecmp(const char *s1, const char *s2)
{
  if (!s1 || !s2)
    return (0);
  while (*s1 != '\0' && *s2 != '\0' && is_same_letter(*s1, *s2))
    {
      ++s1;
      ++s2;
    }
  return (*s1 - *s2);
}
