/*
** specular_effect.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 15:33:39 2015 Aurélien Metz
** Last update Sun Jun  7 20:51:55 2015 huy le
*/

#include "specular_effect.h"

t_color		*specular_effect(const t_ray *const inc_ray,
				 t_scene *const scene,
				 t_obj *const obj,
				 const t_coord *const impact)
{
  return (NULL);
  (void)inc_ray;
  (void)scene;
  (void)obj;
  (void)impact;
}
