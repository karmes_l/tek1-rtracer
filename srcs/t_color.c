/*
** t_color.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu May 28 18:27:36 2015 Aurélien Metz
** Last update Sat Jun  6 13:06:54 2015 Aurélien Metz
*/

#include "color.h"

void	t_color_init(t_color *const color)
{
  if (color)
    {
      color->red = 0;
      color->green = 0;
      color->blue = 0;
    }
}

void	t_color_cpy(t_color *const dest, const t_color *const src)
{
  if (dest && src)
    {
      dest->red = src->red;
      dest->green = src->green;
      dest->blue = src->blue;
    }
}

void		t_color_compute(t_color *const color,
				const float intensity)
{
  unsigned int	colors[3];

  if (color)
    {
      colors[0] = intensity < 1
	? color->red * intensity
	: color->red + (255 - color->red) * (intensity - 1);
      colors[1] = intensity < 1
	? color->green * intensity
	: color->green + (255 - color->green) * (intensity - 1);
      colors[2] = intensity < 1
	? color->blue * intensity
	: color->blue + (255 - color->blue) * (intensity - 1);
      color->red = colors[0] > 255 ? 255 : colors[0];
      color->green = colors[1] > 255 ? 255 : colors[1];
      color->blue = colors[2] > 255 ? 255 : colors[2];
    }
}
