/*
** t_obj.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 17:53:32 2015 Aurélien Metz
** Last update Sun Jun  7 18:35:12 2015 Aurélien Metz
*/

#include "t_obj.h"

/*
** Initialise une structure 't_obj'.
*/
static void	t_obj_init(t_obj *const obj)
{
  if (obj)
    {
      obj->next = NULL;
      t_coord_init(&obj->pos);
      t_coord_init(obj->rotation + 0);
      t_coord_init(obj->rotation + 1);
      t_color_init(&obj->color);
      t_material_init(&obj->material);
      obj->type = OT_VOID;
      obj->dist = 0;
      obj->rayon = 0;
      obj->angle = 0;
      obj->limit[0] = 0;
      obj->limit[1] = 0;
    }
}

/*
** Initialise une structure 't_list_obj'.
*/
void	t_list_obj_init(t_list_obj *const list_obj)
{
  if (list_obj)
    {
      list_obj->size = 0;
      list_obj->head = NULL;
      list_obj->tail = NULL;
    }
}

/*
** Ajoute un élément en fin de liste.
** Retourne 0 sur erreur, 1 sinon.
*/
int	t_list_obj_append(t_list_obj *const list_obj)
{
  t_obj	*node;

  if (!list_obj)
    {
      puterror(T_OBJ_ERR_PARAM);
      return (0);
    }
  if ((node = malloc(sizeof(*node))) == NULL)
    {
      puterror(T_OBJ_ERR_MALLOC);
      return (0);
    }
  t_obj_init(node);
  if (!list_obj->head)
    list_obj->head = node;
  if (list_obj->tail)
    list_obj->tail->next = node;
  list_obj->tail = node;
  ++list_obj->size;
  return (1);
}

/*
** Free les éléments alloués d'une strucutre 't_list_obj'.
*/
void	t_list_obj_destruct(t_list_obj *const list_obj)
{
  t_obj	*tmp;

  if (list_obj != NULL)
    {
      tmp = list_obj->head;
      while (list_obj->head != NULL)
	{
	  list_obj->head = list_obj->head->next;
	  free(tmp);
	  tmp = list_obj->head;
	}
      list_obj->tail = NULL;
      list_obj->size = 0;
    }
}
