/*
** fill_cone.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 28 16:41:58 2015 VIctor Le
** Last update Wed Jun  3 22:40:48 2015 VIctor Le
*/

#include "parser.h"
#include "fill_obj_fct.h"
#include "t_scene.h"
#include "field.h"

/*
** Tous ptrs non-vérifiés: fct static, usage unique
** et réception d'adresse vérifiées.
** Initialise des variables de fill_cone().
*/
static void	fill_cone_init_var(t_obj_fct *fptr, char **fields, int *f_value)
{
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_ANGLE;
  fields[2] = PARSER_FIELD_ROTATION;
  fields[3] = PARSER_FIELD_MATERIAL;
  fields[4] = PARSER_FIELD_COLOR;
  fields[5] = PARSER_FIELD_ABS;
  fields[6] = PARSER_FIELD_LIM;
  fptr[0] = &fill_obj_set_pos;
  fptr[1] = &fill_obj_set_angle;
  fptr[2] = &fill_obj_set_rotation;
  fptr[3] = &fill_obj_set_material;
  fptr[4] = &fill_obj_set_color;
  fptr[5] = &fill_obj_set_abs;
  fptr[6] = &fill_obj_set_bi_lim;
  f_value[0] = F_POS;
  f_value[1] = F_ANGLE;
  f_value[2] = F_ROTATION;
  f_value[3] = F_MATERIAL;
  f_value[4] = F_COLOR;
  f_value[5] = F_ABS;
  f_value[6] = F_LIM;
}

/*
** Tous ptrs non-vérifiés: vérif in fill_cone().
** Deuxième partie de fill_cone().
** Retourne PARSER_OK, PARSER_FAIL.
*/
static int	fill_cone_2(t_scene *scene, char **wdtab, int *line,
			    int *field_filled)
{
  t_obj_fct	fptr[PARSER_CO_FIELDS];
  char		*fields[PARSER_CO_FIELDS];
  int		f_value[PARSER_CO_FIELDS];
  int		ret;
  int		i;

  fill_cone_init_var(fptr, fields, f_value);
  if ((i = get_index(PARSER_CO_FIELDS, fields, wdtab[0])) != PARSER_NO_MATCH)
    {
      ret = fptr[i](scene->list_obj.tail, wdtab, line);
      *field_filled |= f_value[i];
    }
  else
    ret = PARSER_SKIP;
  return (ret);
}

/*
** Tous ptrs non-vérifiés: vérif in parse_line().
** Remplis une structure 't_obj' de 't_scene'.
** Retourne PARSER_OK, PARSER_FAIL, PARSER_WARN, PARSER_SKIP.
*/
int		fill_cone(t_scene *scene, const int fd, int *line, char **s)
{
  int		ret;
  char		**wdtab;
  int		field_filled;

  if (!t_list_obj_append(&scene->list_obj))
    return (PARSER_FAIL);
  scene->list_obj.tail->type = OT_CONE;
  ret = PARSER_OK;
  field_filled = F_VOID;
  while (ret == PARSER_OK)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      ret = fill_cone_2(scene, wdtab, line, &field_filled);
      if (ret != PARSER_SKIP)
	*s = secure_free(*s);
      wdtab = free_wdtab(wdtab);
    }
  if (field_filled == F_VOID || (NEC_FIELD_CO & field_filled) != NEC_FIELD_CO)
    {
      puterror(PARSER_ERR_FIELD(PARSER_KW_CONE));
      ret = PARSER_FAIL;
    }
  return (ret);
}
