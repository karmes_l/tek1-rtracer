/*
** free_wdtab.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 22:57:13 2015 VIctor Le
** Last update Wed May 27 17:06:32 2015 VIctor Le
*/

#include <stdlib.h>

/*
** Free un tableau alloué avec str_to_wdtab.
** Retourne NULL.
*/
void	*free_wdtab(char **wdtab)
{
  int	i;

  if (wdtab)
    {
      i = 0;
      while (wdtab[i])
	{
	  free(wdtab[i]);
	  ++i;
	}
      free(wdtab);
    }
  return (NULL);
}
