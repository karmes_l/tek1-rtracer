/*
** t_rt.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May 24 15:04:38 2015 Aurélien Metz
** Last update Sun Jun  7 20:50:56 2015 huy le
*/

#include "t_rt.h"

int	t_rt_init(t_rt *const rt, void *const scene)
{
  rt->scene = scene;
  if ((rt->mlx_ptr = vgetaddr(mlx_init(), MLX_INIT)) == NULL
      || (rt->win_ptr = vgetaddr(mlx_new_window(rt->mlx_ptr,
						WIDTH, HEIGHT, RT),
				 MLX_NEW_WINDOW)) == NULL
      || t_img_init(rt) == 0)
    return (0);
  return (1);
}
