/*
** t_eye.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:05:37 2015 VIctor Le
** Last update Sun Jun  7 20:45:03 2015 huy le
*/

#include "t_eye.h"

/*
** Initialise à 0 la structure 't_eye'.
*/
void	t_eye_init(t_eye *eye)
{
  if (eye)
    {
      t_coord_init(&eye->pos);
      t_coord_init(&eye->rotation[0]);
      t_coord_init(&eye->rotation[1]);
      t_coord_init(&eye->rotation[2]);
      t_coord_init(&eye->look);
    }
}

/*
** Redéfinit la matrice de rotation par rapport à 'look' est set.
*/
void	t_eye_redef_rotation(t_eye *eye)
{
  if (eye)
    {
      eye->rotation[0].x = cos(CONVERT(eye->look.x))
	* cos(CONVERT(eye->look.y));
      eye->rotation[0].y = -sin(CONVERT(eye->look.y));
      eye->rotation[0].z = cos(CONVERT(eye->look.y))
	* sin(CONVERT(eye->look.x));
      eye->rotation[1].x = cos(CONVERT(eye->look.x))
	* sin(CONVERT(eye->look.y));
      eye->rotation[1].y = cos(CONVERT(eye->look.y));
      eye->rotation[1].z = sin(CONVERT(eye->look.y))
	* sin(CONVERT(eye->look.x));
      eye->rotation[2].x = -sin(CONVERT(eye->look.x));
      eye->rotation[2].y = 0;
      eye->rotation[2].z = cos(CONVERT(eye->look.x));
    }
}
