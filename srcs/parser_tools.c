/*
** parser_tools.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 23:10:20 2015 VIctor Le
** Last update Sat May 30 17:50:34 2015 VIctor Le
*/

#include "parser_tools.h"

/*
** Message d'erreur pour un get_index retournant PARSER_NO_MATCH.
*/
void	print_no_match(const char *const word, const int line)
{
  if (word)
    {
      putwarn(PARSER_ERR_NO_MATCH);
      putwarn(word);
      putwarn(": line ");
      putnbr_warn(line);
      putwarn("\n");
    }
}

/*
** Compare 'word' avec les mots de 'cmp_words'.
** Retourne l'index de 'word' dans 'cmp_words',
**          PARSER_NO_MATCH si non-présent.
*/
int	get_index(const int limit, char **cmp_words, const char *const word)
{
  int	i;

  if (!word || !cmp_words)
    return (get_index_err_param(word, cmp_words));
  i = 0;
  while (i < limit && cmp_words[i] && my_strcasecmp(word, cmp_words[i]))
    ++i;
  if (i == limit)
    return (PARSER_NO_MATCH);
  return (i);
}

/*
** get_next_line jusqu'à erreur, fin de fichier et ligne non-vide.
** Retourne NULL ou ligne non-vide suivante (alloué).
*/
char	*get_next_non_empty_line(const int fd, int *line)
{
  char	*s;

  while ((s = gnl(fd)) && s[0] == '\0')
    {
      if (line)
	++(*line);
      s = secure_free(s);
    }
  if (line)
    ++(*line);
  return (s);
}

/*
** Retourne la taille du wdtab.
*/
int	get_nb_word(char **wdtab)
{
  int	i;

  i = 0;
  if (wdtab)
    {
      while (wdtab[i])
	++i;
    }
  return (i);
}
