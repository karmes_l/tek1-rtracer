/*
** t_scene.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 17:34:26 2015 Aurélien Metz
** Last update Sun Jun  7 18:34:06 2015 Aurélien Metz
*/

#include "t_scene.h"
#include "t_eye.h"
#include "parser.h"

/*
** Parade: filename vérifié in parser().
** Remplis la scene avec les valeurs définies dans le fichier filename.
** Renvoi 0 sur erreur, 1 sinon.
*/
int	t_scene_init(t_scene *scene, const char *const filename)
{
  if (scene)
    {
      t_eye_init(&scene->eye);
      t_list_obj_init(&scene->list_obj);
      t_list_light_init(&scene->list_light);
      if (parser(scene, filename) == PARSER_FAIL)
	{
	  t_scene_destruct(scene);
	  return (0);
	}
      t_eye_redef_rotation(&scene->eye);
    }
  return (1);
}

/*
** Free tous les éléments mallocqués.
*/
void	t_scene_destruct(t_scene *scene)
{
  if (scene)
    {
      t_list_obj_destruct(&scene->list_obj);
      t_list_light_destruct(&scene->list_light);
    }
}
