/*
** rt.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer/srcs
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 14:45:00 2015 Aurélien Metz
** Last update Wed Jun  3 18:32:57 2015 Aurélien Metz
*/

#include "main.h"

int		main(const int ac, const char *const *const av,
		     const char *const *const env)
{
  t_rt		rt;
  t_scene	scene;

  if (error_managment(ac, env) == 0
      || t_scene_init(&scene, av[1]) == 0
      || t_rt_init(&rt, &scene) == 0)
    return (-1);
  draw(&rt, &scene);
  mlx_expose_hook(rt.win_ptr, &expose, &rt);
  mlx_key_hook(rt.win_ptr, &key, &rt);
  mlx_loop(rt.mlx_ptr);
  return (0);
}
