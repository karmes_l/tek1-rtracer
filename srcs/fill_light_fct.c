/*
** fill_light_fct.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 28 15:34:54 2015 VIctor Le
** Last update Sun Jun  7 21:38:57 2015 huy le
*/

#include "parser.h"
#include "fill_fct_tools.h"

/*
** Tous ptrs non-vérifiés: vérification in fill_light().
** Replis la variable 'pos' de 't_light'.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int		fill_light_set_pos(t_light *light, char **wdtab, int *line)
{
  int		i;
  float		*tmp[3];

  if (check_nb_field_args(PARSER_NB_ARG_POS, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  tmp[0] = &light->pos.x;
  tmp[1] = &light->pos.y;
  tmp[2] = &light->pos.z;
  i = 1;
  while (i < PARSER_NB_ARG_POS)
    {
      (*tmp)[i - 1] = atof(wdtab[i]);
      ++i;
    }
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifiés: vérification in fill_light().
** Set la variable 'intensity' de 't_light'.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_light_set_intensity(t_light *light, char **wdtab, int *line)
{
  float	tmp;

  if (check_nb_field_args(PARSER_NB_ARG_INTST, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  if ((tmp = atof(wdtab[1])) < 1)
    {
      putwarn(PARSER_WARN_NEG_VALUE);
      tmp = 0;
    }
  light->intensity = tmp;
  return (PARSER_OK);
}
