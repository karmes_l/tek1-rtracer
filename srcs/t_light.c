/*
** t_light.c for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 18:10:20 2015 Aurélien Metz
** Last update Sun Jun  7 21:33:41 2015 huy le
*/

#include <stdlib.h>
#include "light.h"
#include "t_coord.h"
#include "t_scene.h"
#include "error_managment.h"

/*
** Initialise une structure 't_light' à 0.
*/
static void	t_light_init(t_light *light)
{
  if (light)
    {
      light->next = NULL;
      t_coord_init(&light->pos);
      light->intensity = 0;
    }
}

/*
** Initialise une structure 't_list_light' à 0.
*/
void	t_list_light_init(t_list_light *const list_light)
{
  if (list_light)
    {
      list_light->size = 0;
      list_light->head = NULL;
      list_light->tail = NULL;
    }
}

/*
** Ajoute un élément en fin de liste.
** Retourne 1 (OK) ou 0 (FAIL).
*/
int		t_list_light_append(t_list_light *const list_light)
{
  t_light	*node;

  if (!list_light)
    {
      puterror(T_LIGHT_ERR_PARAM);
      return (0);
    }
  if ((node = malloc(sizeof(*node))) == NULL)
    {
      puterror(T_LIGHT_ERR_MALLOC);
      return (0);
    }
  t_light_init(node);
  if (!list_light->head)
    list_light->head = node;
  if (list_light->tail)
    list_light->tail->next = node;
  list_light->tail = node;
  ++list_light->size;
  return (1);
}

/*
** Free les éléments alloués d'une structure 't_list_light'.
*/
void		t_list_light_destruct(t_list_light *list_light)
{
  t_light	*tmp;

  if (list_light != NULL)
    {
      tmp = list_light->head;
      while (list_light->head != NULL)
	{
	  list_light->head = list_light->head->next;
	  free(tmp);
	  tmp = list_light->head;
	}
      list_light->tail = NULL;
      list_light->size = 0;
    }
}
