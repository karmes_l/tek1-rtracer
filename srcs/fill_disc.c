/*
** fill_disc.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon Jun  1 18:01:42 2015 VIctor Le
** Last update Wed Jun  3 22:37:22 2015 VIctor Le
*/

#include "parser.h"
#include "t_scene.h"
#include "field.h"
#include "fill_obj_fct.h"

/*
** Tous ptrs non-vérifs: Vérif in fill_disc_2().
** Initialise les variables de fill_disc_2().
*/
static void	fill_disc_init_var(t_obj_fct *fptr, char **fields, int *f_value)
{
  fptr[0] = &fill_obj_set_pos;
  fptr[1] = &fill_obj_set_rayon;
  fptr[2] = &fill_obj_set_bi_rotation;
  fptr[3] = &fill_obj_set_material;
  fptr[4] = &fill_obj_set_abs;
  fptr[5] = &fill_obj_set_color;
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_RAYON;
  fields[2] = PARSER_FIELD_ROTATION;
  fields[3] = PARSER_FIELD_MATERIAL;
  fields[4] = PARSER_FIELD_ABS;
  fields[5] = PARSER_FIELD_COLOR;
  f_value[0] = F_POS;
  f_value[1] = F_RAYON;
  f_value[2] = F_ROTATION;
  f_value[3] = F_MATERIAL;
  f_value[4] = F_ABS;
  f_value[5] = F_COLOR;
}

/*
** Tous ptrs non-vérifs: Vérif fill_disc().
** Deuxième partie de fill_disc().
** Retourne PARSER_OK, PARSER_FAIL, PARSER_SKIP.
*/
static int	fill_disc_2(t_scene *scene, char **wdtab, int *line,
			    int *field_filled)
{
  t_obj_fct	fptr[PARSER_DI_FIELDS];
  char		*fields[PARSER_DI_FIELDS];
  int		f_value[PARSER_DI_FIELDS];
  int		ret;
  int		i;

  fill_disc_init_var(fptr, fields, f_value);
  if ((i = get_index(PARSER_DI_FIELDS, fields, wdtab[0])) != PARSER_NO_MATCH)
    {
      ret = fptr[i](scene->list_obj.tail, wdtab, line);
      *field_filled |= f_value[i];
    }
  else
    ret = PARSER_SKIP;
  return (ret);
}

/*
** Tous ptrs non-vérifs: Vérif in parser_line().
** Remplis un 't_obj' comme un disque dans la liste 't_obj' de 'scene'
** Retourne PARSER_OK, PARSER_FAIL, PARSER_SKIP, PARSER_END.
*/
int	fill_disc(t_scene *scene, const int fd, int *line, char **s)
{
  char	**wdtab;
  int	ret;
  int	field_filled;

  if (!t_list_obj_append(&scene->list_obj))
    return (PARSER_FAIL);
  scene->list_obj.tail->type = OT_DISC;
  field_filled = F_VOID;
  ret = PARSER_OK;
  while (ret == PARSER_OK)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      ret = fill_disc_2(scene, wdtab, line, &field_filled);
      if (ret != PARSER_SKIP)
	*s = secure_free(*s);
      wdtab = free_wdtab(wdtab);
    }
  if (field_filled == F_VOID || (NEC_FIELD_DI & field_filled) == NEC_FIELD_DI)
    {
      puterror(PARSER_ERR_FIELD(PARSER_KW_DISC));
      ret = PARSER_FAIL;
    }
  return (ret);
}
