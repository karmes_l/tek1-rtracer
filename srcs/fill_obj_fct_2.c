/*
** fill_obj_fct_2.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed Jun  3 22:25:28 2015 VIctor Le
** Last update Wed Jun  3 22:39:04 2015 VIctor Le
*/

#include "parser.h"
#include "fill_fct_tools.h"

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set le 'rayon' d'un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_rayon(t_obj *obj, char **wdtab, int *line)
{
  if (check_nb_field_args(PARSER_NB_ARG_RAYON, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  if ((obj->rayon = atof(wdtab[1])) < 1)
    {
      puterror(PARSER_ERR_BAD_RAYON);
      return (PARSER_FAIL);
    }
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set l'absorption d'un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_abs(t_obj *obj, char **wdtab, int *line)
{
  float	tmp;

  if (check_nb_field_args(PARSER_NB_ARG_ABS, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  if ((tmp = atof(wdtab[1])) < 0)
    {
      putwarn(PARSER_WARN_NEG_VALUE);
      tmp = 0;
    }
  obj->material.absorption = (unsigned char)tmp;
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set 1 limite à un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_lim(t_obj *obj, char **wdtab, int *line)
{
  float	tmp;
  int	i;

  if (check_nb_field_args(PARSER_NB_ARG_LIM, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  i = 1;
  while (i < PARSER_NB_ARG_LIM)
    {
      if ((tmp = atof(wdtab[i])) < 0)
	{
	  puterror(PARSER_WARN_NEG_VALUE);
	  return (PARSER_FAIL);
	}
      obj->limit[i - 1] = (unsigned int)tmp;
      ++i;
    }
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérif in fill_<obj>().
** Set 2 limites à un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_bi_lim(t_obj *obj, char **wdtab, int *line)
{
  float	tmp;
  int	i;

  if (check_nb_field_args(PARSER_NB_ARG_BI_LIM, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  i = 1;
  while (i < PARSER_NB_ARG_BI_LIM)
    {
      if ((tmp = atof(wdtab[i])) < 0)
	{
	  puterror(PARSER_WARN_NEG_VALUE);
	  return (PARSER_FAIL);
	}
      obj->limit[i - 1] = (unsigned int)tmp;
      ++i;
    }
  return (PARSER_OK);
}

/*
** Tous ptrs non-vérifs: Vérifs in fill_<obj>().
** Set deux vecteurs de rotation d'un objet.
** Retourne PARSER_OK, PARSER_FAIL.
*/
int	fill_obj_set_bi_rotation(t_obj *obj, char **wdtab, int *line)
{
  float	*tmp[6];
  int	i;

  if (check_nb_field_args(PARSER_NB_ARG_BI_ROT, wdtab, line) == PARSER_FAIL)
    return (PARSER_FAIL);
  tmp[0] = &obj->rotation[0].x;
  tmp[1] = &obj->rotation[0].y;
  tmp[2] = &obj->rotation[0].z;
  tmp[3] = &obj->rotation[1].x;
  tmp[4] = &obj->rotation[1].y;
  tmp[5] = &obj->rotation[1].z;
  i = 1;
  while (i < PARSER_NB_ARG_BI_ROT)
    {
      (*tmp)[i - 1] = atof(wdtab[i]);
      ++i;
    }
  if (check_null_rotation(&obj->rotation[0]) == PARSER_FAIL
      || check_null_rotation(&obj->rotation[1]) == PARSER_FAIL)
    return (PARSER_FAIL);
  return (PARSER_OK);
}
