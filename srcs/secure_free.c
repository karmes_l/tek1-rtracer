/*
** secure_free.c for RT in /home/le_l/rendu/Igraph/MUL_2014_rtracer
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 08:38:08 2015 VIctor Le
** Last update Wed Jun  3 18:29:47 2015 Aurélien Metz
*/

#include <stdlib.h>

/*
** Vérifie que le pointeur n'est pas NULL et le free.
** Pas besoin de cast. Segfault toujours possible.
** Retourne NULL.
*/
void	*secure_free(void *ptr)
{
  if (ptr)
    {
      free(ptr);
      ptr = NULL;
    }
  return (ptr);
}
