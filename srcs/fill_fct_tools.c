/*
** fill_fct_tools.c for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed May 27 13:40:29 2015 VIctor Le
** Last update Sun Jun  7 20:54:23 2015 huy le
*/

#include <stdlib.h>
#include "str_to_wdtab.h"
#include "parser_tools.h"
#include "parser_errors.h"

/*
** Retourne un wdtab contenant au moins 1 mot.
** NULL sur erreur ou tableau vide.
*/
char	**fill_fct_set_wdtab(char **s, int *ret, const int fd, int *line)
{
  char	**wdtab;

  if (!s || !ret || !line)
    return (NULL);
  if (!(*s = get_next_non_empty_line(fd, line))
      || !(wdtab = str_to_wdtab(*s, " \t")))
    {
      *ret = (!*s) ? PARSER_END : PARSER_FAIL;
      *s = secure_free(*s);
      return (NULL);
    }
  if (wdtab && wdtab[0] == NULL)
    {
      wdtab = free_wdtab(wdtab);
      *ret = PARSER_END;
    }
  return (wdtab);
}

/*
** Vérifie que le nb minimum requis d'argument pour un champ soit présent.
** Retourne PARSER_FAIL, PARSER_OK, PARSER_SKIP.
*/
int	check_nb_field_args(const int min_arg, char **wdtab, int *line)
{
  int	size_wdtab;

  if (!wdtab || !line)
    return (PARSER_SKIP);
  size_wdtab = get_nb_word(wdtab);
  if (size_wdtab < min_arg)
    return (parser_err_warn(PARSER_ERR_FEW_ARG, *line, PARSER_FAIL));
  else if (size_wdtab > min_arg)
    (void)parser_err_warn(PARSER_WARN_MANY_ARG, *line, PARSER_WARN);
  return (PARSER_OK);
}

/*
** Vérifie qu'un vecteur de rotation est nul.
** Retourne PARSER_FAIL s'il l'est, PARSER_OK sinon.
*/
int	check_null_rotation(t_coord *rotation)
{
  if (rotation)
    {
      if (rotation->x == 0 && rotation->y == 0 && rotation->z == 0)
	{
	  puterror(PARSER_ERR_NULL_ROT);
	  return (PARSER_FAIL);
	}
    }
  return (PARSER_OK);
}
