/*
** sphere.c<2> for sphere in /home/le_n/rendu/igraph/rtv1_metz/MUL_2014_rtv1/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Wed May 27 17:26:18 2015 huy le
** Last update Sat Jun  6 15:40:53 2015 Aurélien Metz
*/

#include "sphere.h"

/*
** ray et obj non checké : verification des ptr en amont
*/
void	sphere(const t_ray *const ray, t_obj *const obj)
{
  float	delta;
  float	equation[3];
  float	dist[2];

  obj->dist = 0;
  equation[0] = P2(norm(&ray->pos[1]));
  equation[1] = 2 * (ray->pos[1].x * (ray->pos->x - obj->pos.x)
		     + ray->pos[1].y * (ray->pos->y - obj->pos.y)
		     + ray->pos[1].z * (ray->pos->z - obj->pos.z));
  equation[2] = P2(ray->pos[0].x - obj->pos.x)
    + P2(ray->pos[0].y - obj->pos.y)
    + P2(ray->pos[0].z - obj->pos.z)
    - P2(obj->rayon);
  if ((delta = DELTA(equation[0], equation[1], equation[2])) >= 0)
    {
      dist[0] = (-equation[1] - sqrt(delta)) / (2 * equation[0]);
      dist[1] = (-equation[1] + sqrt(delta)) / (2 * equation[0]);
      obj->dist = MIN_POS(dist[0], dist[1]);
    }
}
