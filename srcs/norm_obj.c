/*
** norm_obj.c for rt in /home/le_l/workspace/rt/texte
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu Jun  4 18:21:19 2015 VIctor Le
** Last update Sun Jun  7 21:01:15 2015 huy le
*/

#include "norm_obj.h"

void	norm_vect_sphere(t_coord *const normal, const t_obj *const obj,
			 const t_coord *const impact)
{
  if (normal && obj && impact)
    {
      normal->x = impact->x - obj->pos.x;
      normal->y = impact->y - obj->pos.y;
      normal->z = impact->z - obj->pos.z;
    }
  else
    puterror("norm_vect_sphere: Internal error\n");
}

void		norm_vect_cone(t_coord *const normal, const t_obj *const obj,
			       const t_coord *const impact)
{
  t_coord	v_co_impact;
  t_coord	pv_v_co_imp_vco;

  if (normal && obj && impact)
    {
      v_co_impact.x = impact->x - obj->pos.x;
      v_co_impact.y = impact->y - obj->pos.y;
      v_co_impact.z = impact->z - obj->pos.z;
      pvector(&v_co_impact, &obj->rotation[0], &pv_v_co_imp_vco);
      pvector(&obj->rotation[0], &pv_v_co_imp_vco, normal);
    }
  else
    puterror("norm_vect_cone: Internal error\n");
}

void		norm_vect_cy(t_coord *const normal, const t_obj *const obj,
			     const t_coord *const impact)
{
  t_coord	v_cy_impact;
  t_coord	pv_v_cy_imp_vcy;

  if (normal && obj && impact)
    {
      v_cy_impact.x = impact->x - obj->pos.x;
      v_cy_impact.y = impact->y - obj->pos.y;
      v_cy_impact.z = impact->z - obj->pos.z;
      pvector(&obj->rotation[0], &v_cy_impact, &pv_v_cy_imp_vcy);
      pvector(&pv_v_cy_imp_vcy, &obj->rotation[0], normal);
    }
  else
    puterror("norm_vect_cy: Internal error\n");
}

void		norm_vect_plan(t_coord *const normal, const t_obj *const obj,
			       const t_coord *const impact)
{
  (void)impact;
  if (normal && obj)
    pvector(&obj->rotation[0], &obj->rotation[1], normal);
  else
    puterror("norm_vect_plan: Internal error\n");
}
