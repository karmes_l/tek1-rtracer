/*
** fill_cylinder.c for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 28 21:18:14 2015 VIctor Le
** Last update Wed Jun  3 22:31:37 2015 VIctor Le
*/

#include "parser.h"
#include "t_scene.h"
#include "fill_obj_fct.h"
#include "field.h"

/*
** Tous ptrs non-vérifs: Vérif in fill_cylinder().
** Initialise des variables de fill_cylinder().
*/
static void	fill_cylinder_init_var(t_obj_fct *fptr, char **fields,
				       int *f_value)
{
  fields[0] = PARSER_FIELD_POS;
  fields[1] = PARSER_FIELD_RAYON;
  fields[2] = PARSER_FIELD_ROTATION;
  fields[3] = PARSER_FIELD_MATERIAL;
  fields[4] = PARSER_FIELD_COLOR;
  fields[5] = PARSER_FIELD_ABS;
  fields[6] = PARSER_FIELD_LIM;
  fptr[0] = &fill_obj_set_pos;
  fptr[1] = &fill_obj_set_rayon;
  fptr[2] = &fill_obj_set_rotation;
  fptr[3] = &fill_obj_set_material;
  fptr[4] = &fill_obj_set_color;
  fptr[5] = &fill_obj_set_abs;
  fptr[6] = &fill_obj_set_lim;
  f_value[0] = F_POS;
  f_value[1] = F_RAYON;
  f_value[2] = F_ROTATION;
  f_value[3] = F_MATERIAL;
  f_value[4] = F_COLOR;
  f_value[5] = F_ABS;
  f_value[6] = F_LIM;
}

/*
** Tous trs non-vérifs: Vérif in fill_cylinder().
** Deuxième partie de fill_cylinder().
** Retourne PARSER_OK, PARSER_FAIL, PARSER_SKIP.
*/
static int	fill_cylinder_2(t_scene *scene, char **wdtab, int *line,
				int *field_filled)
{
  t_obj_fct	fptr[PARSER_CY_FIELDS];
  char		*fields[PARSER_CY_FIELDS];
  int		f_value[PARSER_CY_FIELDS];
  int		i;
  int		ret;

  fill_cylinder_init_var(fptr, fields, f_value);
  if ((i = get_index(PARSER_CY_FIELDS, fields, wdtab[0])) != PARSER_NO_MATCH)
    {
      ret = fptr[i](scene->list_obj.tail, wdtab, line);
      *field_filled |= f_value[i];
    }
  else
    ret = PARSER_SKIP;
  return (ret);
}

/*
** Tous ptrs non-vérifs: Vérif in parse_line().
** Remplis une structure objet en le considérant comme un cylindre.
** Retourne PARSER_OK, PARSER_WARN, PARSER_FAIL, PARSER_SKIP.
*/
int		fill_cylinder(t_scene *scene, const int fd, int *line, char **s)
{
  int		ret;
  char		**wdtab;
  int		field_filled;

  if (!t_list_obj_append(&scene->list_obj))
    return (PARSER_FAIL);
  scene->list_obj.tail->type = OT_CYLINDER;
  ret = PARSER_OK;
  field_filled = F_VOID;
  while (ret == PARSER_OK)
    {
      if (!(wdtab = fill_fct_set_wdtab(s, &ret, fd, line)))
	return (ret);
      ret = fill_cylinder_2(scene, wdtab, line, &field_filled);
      if (ret != PARSER_SKIP)
	*s = secure_free(*s);
      wdtab = free_wdtab(wdtab);
    }
  if (field_filled == F_VOID || (NEC_FIELD_CY & field_filled) != NEC_FIELD_CY)
    {
      puterror(PARSER_ERR_FIELD(PARSER_KW_CYLINDER));
      ret = PARSER_FAIL;
    }
  return (ret);
}
