##
## Makefile for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
##
## Made by Aurélien Metz
## Login   <metz_a@epitech.net>
##
## Started on  Sun Jun  7 12:50:24 2015 Aurélien Metz
## Last update Sun Jun  7 18:38:13 2015 Aurélien Metz
##

################################################################################
CC	=	gcc
RM	=	rm  -f
NAME	=	rt
CFLAGS	+=	-Werror -Wall -Wextra
CFLAGS	+=	-ansi -pedantic
CFLAGS	+=	-I include
LDFLAGS	=	-L . -L /usr/lib64 -lmlx -lXext -lX11 -lmlx_x86_64 -lm
OBJS	=	$(SRCS:.c=.o)
################################################################################
SRCS	=	srcs/calcul.c			\
		srcs/cone.c			\
		srcs/cylindre.c			\
		srcs/draw.c			\
		srcs/error_managment.c		\
		srcs/event.c			\
		srcs/fill_cone.c		\
		srcs/fill_cylinder.c		\
		srcs/fill_disc.c		\
		srcs/fill_eye.c			\
		srcs/fill_fct_tools.c		\
		srcs/fill_light.c		\
		srcs/fill_light_fct.c		\
		srcs/fill_para.c		\
		srcs/fill_plan.c		\
		srcs/fill_sphere.c		\
		srcs/fill_triangle.c		\
		srcs/free_wdtab.c		\
		srcs/get_normal.c		\
		srcs/gnl.c			\
		srcs/intersection.c		\
		srcs/light_managment.c		\
		srcs/main.c			\
		srcs/my_strcasecmp.c		\
		srcs/norm_obj.c			\
		srcs/parser.c			\
		srcs/parser_errors.c		\
		srcs/parser_tools.c		\
		srcs/plan.c			\
		srcs/raytracer.c		\
		srcs/secure_free.c		\
		srcs/specular_effect.c		\
		srcs/sphere.c			\
		srcs/str2.c			\
		srcs/str.c			\
		srcs/str_to_wdtab.c		\
		srcs/t_color.c			\
		srcs/t_coord.c			\
		srcs/t_eye.c			\
		srcs/t_img.c			\
		srcs/t_light.c			\
		srcs/t_material.c		\
		srcs/t_obj.c			\
		srcs/t_ray.c			\
		srcs/t_rt.c			\
		srcs/t_scene.c			\
		srcs/fill_obj_fct.c		\
		srcs/fill_obj_fct_2.c		\
################################################################################
all:		$(NAME)

$(NAME):	$(OBJS)
		$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

clean:
		$(RM) $(OBJS)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
################################################################################
