/*
** color.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 14:39:00 2015 Aurélien Metz
** Last update Wed Jun  3 18:52:41 2015 Aurélien Metz
*/

#ifndef COLOR_H_
# define COLOR_H_

/*
** @red:	Composante rouge	(%)
** @green:	Composante vert		(%)
** @blue:	Composante bleu		(%)
*/
typedef struct	s_color
{
  unsigned char	red;
  unsigned char	green;
  unsigned char	blue;
}		t_color;

#endif /* !COLOR_H_ */
