/*
** light_managment.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu May 28 18:07:07 2015 Aurélien Metz
** Last update Sun Jun  7 00:10:15 2015 Aurélien Metz
*/

#ifndef LIGHT_MANAGMENT_H_
# define LIGHT_MANAGMENT_H_

# include <math.h>
# include "win_size.h"
# include "scene.h"
# include "formules.h"
# include "ray.h"

# define CURS		(100)

void	t_coord_cpy(t_coord *const, const t_coord *const);
float	scalaire(const t_coord *const, const t_coord *const);
float	norm(const t_coord *const);
void	get_normal(t_coord *const, const t_obj *const, const t_coord *const);
t_obj	*search_shortest_dist(t_obj *, const t_ray *const,
			      const float, const float);

#endif /* !LIGHT_MANAGMENT_H_ */
