/*
** t_ray.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Fri Jun  5 19:40:47 2015 Aurélien Metz
** Last update Fri Jun  5 19:43:55 2015 Aurélien Metz
*/

#ifndef T_RAY_H_
# define T_RAY_H_

# include "ray.h"

void	t_coord_init(t_coord *const);

#endif /* !T_RAY_H */
