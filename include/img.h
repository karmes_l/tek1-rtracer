/*
** img.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May 24 13:43:20 2015 Aurélien Metz
** Last update Mon May 25 17:08:45 2015 melvin mohadeb
*/

#ifndef IMG_H_
# define IMG_H_

/*
** @img_ptr:	Pointeur sur l'image (mlx)
** @data:	Pointeur sur le data de l'image
** @bpp:	Nombre de bits par pixel
** @size_line	Nombre d'octets par ligne (de l'image)
** @endian	indicateur sur l'endian
*/
typedef struct	s_img
{
  void		*img_ptr;
  char		*data;
  int		bpp;
  int		size_line;
  int		endian;
}		t_img;

#endif /* !IMG_H_ */
