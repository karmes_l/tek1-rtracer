/*
** draw.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 14:14:30 2015 Aurélien Metz
** Last update Sun Jun  7 22:57:54 2015 huy le
*/

#ifndef DRAW_H_
# define DRAW_H_

# include <stdlib.h>
# include "win_size.h"
# include "color.h"
# include "scene.h"
# include "rt.h"

# define D		(3000)

t_color		*raytracer(t_scene *const, const unsigned int,
			   const unsigned int);

#endif /* !DRAW_H_ */
