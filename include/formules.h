/*
** formules.h for  in /home/karmes_l/Projets/Igraph/raytracer/MUL_2014_rtracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May 27 18:20:30 2015 lionel karmes
** Last update Sun Jun  7 23:07:09 2015 VIctor Le
*/

#ifndef FORMULES_H_
# define FORMULES_H_

# define PI			(3.14159265359)
# define P2(X)			((X) * (X))
# define DELTA(A, B, C)		(P2(B) - (4 * A * C))
# define MAX(a, b)		((a) > (b) ? (a) : (b))
# define MIN(a, b)		((a) < (b) ? (a) : (b))
# define IS_A_POS(a, b)		((a) < (b) && (a > 0.01))
# define B_NEG(a, b)		((a) > 0.01 && (b) <= 0.01)
# define MIN_POS(a, b)		(IS_A_POS(a, b) || B_NEG(a, b) ? (a) : (b))
# define ABS(x)			((x) < 0 ? (-x) : (x))
# define CONVERT(a)		((a * PI) / 180)

#endif /* !FORMULES_H_ */
