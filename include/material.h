/*
** material.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 15:36:20 2015 VIctor Le
** Last update Sun Jun  7 00:24:27 2015 Aurélien Metz
*/

#ifndef MATERIAL_H_
# define MATERIAL_H_

enum	e_mat_type
  {
    MT_VOID = 0,
    MT_BROAD_CAST,
    MT_TRANSMIT,
    MT_REFLECT
  };

typedef struct	s_material
{
  unsigned char	type;
  float		absorption;
}		t_material;

#endif /* !MATERIAL_H_ */
