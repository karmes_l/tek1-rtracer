/*
** parser_tools.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 23:10:30 2015 VIctor Le
** Last update Wed May 27 21:49:46 2015 VIctor Le
*/

#ifndef PARSER_TOOLS_H_
# define PARSER_TOOLS_H_

# include <stdlib.h>
# include "parser_errors.h"
# include "gnl.h"
# include "my_strcasecmp.h"
# include "str_to_wdtab.h"
# include "free_wdtab.h"
# include "secure_free.h"

void	print_no_match(const char *const word, const int line);
int	get_index(const int limit, char **cmp_words, const char *const word);
char	*get_next_non_empty_line(const int fd, int *line);
int	get_nb_word(char **wdtab);
char	**fill_fct_set_wdtab(char **s, int *ret, const int fd, int *line);

#endif /* !PARSER_TOOLS_H_ */
