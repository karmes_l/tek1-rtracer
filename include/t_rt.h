/*
** t_rt.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 15:22:04 2015 Aurélien Metz
** Last update Sat Jun  6 13:30:21 2015 Aurélien Metz
*/

#ifndef T_RT_H_
# define T_RT_H_

# include <stdlib.h>
# include "win_size.h"
# include "scene.h"
# include "rt.h"
# include "mlx.h"

# define RT		("Ray Tracer")
# define MLX_INIT	("\033[31mFailed to initiate mlx environement\n\033[0m")
# define MLX_NEW_WINDOW	("\033[31mFailed to initiate a new window\n\033[0m")

void	*vgetaddr(void * const, const char * const);
void	t_scene_destruct(t_scene *const);
int	t_img_init(t_rt *const);
int	t_scene_init(t_scene *const, const char * const);

#endif /* !T_RT_H_ */
