/*
** plan.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu Jun  4 19:29:57 2015 Aurélien Metz
** Last update Sat Jun  6 17:14:30 2015 Aurélien Metz
*/

#ifndef PLAN_H_
# define PLAN_H_

# include "ray.h"
# include "obj.h"

void	pvector(const t_coord *const, const t_coord *const, t_coord *const);
float	scalaire(const t_coord *const, const t_coord *const);

#endif /* !PLAN_H_ */
