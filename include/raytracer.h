/*
** raytracer.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu May 28 18:21:04 2015 Aurélien Metz
** Last update Sun Jun  7 20:30:47 2015 huy le
*/

#ifndef RAYTRACER_H_
# define RAYTRACER_H_

# include <stdlib.h>
# include "ray.h"
# include "win_size.h"
# include "scene.h"

# define D		(3000)

void		t_ray_init(t_ray *const);
void		t_coord_init(t_coord *const);
void		t_color_init(t_color *const);
void		t_color_cpy(t_color *const, const t_color *const);
void		t_color_compute(t_color *const, const unsigned int);
unsigned int	ilumination(const t_coord *const, t_scene *const,
			    t_ray *const, const t_obj *const);
t_color		*specular_effect(t_ray *const, t_scene *const,
				 t_obj *const, const t_coord *const);
void		intersection(t_obj *const, const t_ray *const,
			     const float, const float);

#endif /* !RAYTRACER_H_ */
