/*
** norm_obj.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu Jun  4 19:48:16 2015 Aurélien Metz
** Last update Sun Jun  7 10:51:27 2015 Aurélien Metz
*/

#ifndef NORM_OBJ_H_
# define NORM_OBJ_H_

# include "obj.h"

float	scalaire(const t_coord *const, const t_coord *const);
void	pvector(const t_coord *const, const t_coord *const, t_coord *const);
void	puterror(const char *const);

#endif /* !NORM_OBJ_H_ */
