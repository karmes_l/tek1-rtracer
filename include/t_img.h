/*
** t_img.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 16:58:38 2015 Aurélien Metz
** Last update Sun Jun  7 21:30:19 2015 huy le
*/

#ifndef T_IMG_H_
# define T_IMG_H_

# include <stdlib.h>
# include "win_size.h"
# include "rt.h"
# include "mlx.h"

# define MLX_NEW_IMAGE		("Failed to initiate a new image\n")
# define MLX_GET_DATA_ADDR	("Failed to get data's image address\n")

void	*vgetaddr(void * const, const char * const);

#endif /* !T_IMG_H_ */
