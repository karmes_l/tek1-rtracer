/*
** light.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 14:26:47 2015 Aurélien Metz
** Last update Sun Jun  7 00:02:10 2015 Aurélien Metz
*/

#ifndef LIGHT_H_
# define LIGHT_H_

# include "coord.h"

/*
** @pos:	Coordonées du spot lumineux
** @intensity:	Indicateur sur l'intensitée du spot lumineux
** @next:	Pointeur sur le prochain objet
*/
typedef struct		s_light
{
  struct s_light	*next;
  t_coord		pos;
  float			intensity;
}			t_light;

/*
** @size:	Indcateur sur la taille de la liste
** @head:	Pointeur sur le premier objet
** @tail:	Poitneur sur le dernier objet
*/
typedef struct	s_list_light
{
  t_light	*head;
  t_light	*tail;
  unsigned int	size;
}		t_list_light;

#endif /* !LIGHT */
