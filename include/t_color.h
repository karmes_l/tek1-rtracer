/*
** t_color.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed May 27 11:58:02 2015 VIctor Le
** Last update Wed May 27 12:03:23 2015 VIctor Le
*/

#ifndef T_COLOR_H_
# define T_COLOR_H_

# include "color.h"

void	t_color_init(t_color *color);

#endif /* !T_COLOR_H_ */
