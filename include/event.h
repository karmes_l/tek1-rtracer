/*
** event.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May 24 15:55:59 2015 Aurélien Metz
** Last update Sun May 31 17:57:11 2015 Aurélien Metz
*/

#ifndef EVENT_H_
# define EVENT_H_

# include <stdlib.h>
# include "scene.h"
# include "mlx.h"
# include "rt.h"

# define ESC		(65307)

void	t_scene_destruct(t_scene *const);

#endif /* !EVENT_H_ */
