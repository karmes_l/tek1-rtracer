/*
** cylindre.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 16:46:15 2015 Aurélien Metz
** Last update Sun Jun  7 20:30:15 2015 huy le
*/

#ifndef CYLINDRE_H_
# define CYLINDRE_H_

# include <math.h>
# include "ray.h"
# include "obj.h"
# include "formules.h"

float	norm(t_coord *const);
float	scalaire(const t_coord *const, const t_coord *const);
void	pvector(const t_coord *const, const t_coord *const, t_coord *const);
void	t_coord_init(t_coord *const);
void	get_impact(t_coord *const, const t_ray *const, const float);

#endif /* !CYLINDRE_H_ */
