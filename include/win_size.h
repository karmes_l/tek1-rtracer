/*
** win_size.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu May 28 18:00:01 2015 Aurélien Metz
** Last update Sat Jun  6 13:28:15 2015 Aurélien Metz
*/

#ifndef WIN_SIZE_H_
# define WIN_SIZE_H_

# define WIDTH		(1900)
# define HEIGHT		(1000)

#endif /* !WIN_SIZE_H_ */
