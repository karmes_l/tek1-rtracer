/*
** eye.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 13:56:59 2015 Aurélien Metz
** Last update Tue May 26 12:03:53 2015 VIctor Le
*/

#ifndef EYE_H_
# define EYE_H_

# include "coord.h"

/*
** @pos:	Coordonées de l'oeil
** @rotation	Matrice de rotation de la droite de vision
** @look:	Angles de définition du regard
*/
typedef struct	s_eye
{
  t_coord	pos;
  t_coord	rotation[3];
  t_coord	look;
}		t_eye;

#endif /* !EYE_H_ */
