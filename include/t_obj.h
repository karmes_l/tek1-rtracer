/*
** t_obj.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu Jun  4 19:38:52 2015 Aurélien Metz
** Last update Thu Jun  4 19:42:38 2015 Aurélien Metz
*/

#ifndef T_OBJ_H_
# define T_OBJ_H_

#include <stdlib.h>
#include "t_scene.h"

void	puterror(const char *const);
void	t_color_init(t_color *const);
void	t_coord_init(t_coord *const);
void	t_material_init(t_material *const);

#endif /* !T_OBJ_H_ */
