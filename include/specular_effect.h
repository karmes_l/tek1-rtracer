/*
** specluar_effect.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun Jun  7 16:09:38 2015 Aurélien Metz
** Last update Sun Jun  7 21:29:47 2015 huy le
*/

#ifndef SPECULAR_EFFECT_H_
# define SPECULAR_EFFECT_H_

#include <stdlib.h>
#include "scene.h"
#include "ray.h"

void	t_coord_cpy(t_coord *const, const t_coord *const);
float	scalaire(const t_coord *const, const t_coord *const);
t_color	*send_ray(t_ray *const, t_scene *const);
void	get_normal(t_coord *const, const t_obj *const,
		   const t_coord *const);

#endif /* !SPECULAR_EFFECT_H_ */
