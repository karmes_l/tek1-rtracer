/*
** intersection.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 17:08:51 2015 Aurélien Metz
** Last update Sat Jun  6 16:48:34 2015 Aurélien Metz
*/

#ifndef INTERSECTION_H_
# define INTERSECTION_H_

# include <stdlib.h>
# include "nbr_obj.h"
# include "obj.h"
# include "ray.h"

void	sphere(const t_ray *const, t_obj *const);
void	cylindre(const t_ray *const, t_obj *const);
void	cone(const t_ray *const, t_obj *const);
void	plan(const t_ray *const, t_obj *const);

#endif /* !INTERSECTION_H_ */
