/*
** my_strcasecmp.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 08:41:14 2015 VIctor Le
** Last update Sun Jun  7 21:28:25 2015 huy le
*/

#ifndef MY_STRCASECMP_H_
# define MY_STRCASECMP_H_

int	my_strcasecmp(const char *s1, const char *s2);

#endif /* !MY_STRCASECMP_H_ */
