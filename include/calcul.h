/*
** calcul.h for  in /home/karmes_l/Projets/Igraph/raytracer/MUL_2014_rtracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May 27 18:34:14 2015 lionel karmes
** Last update Wed Jun  3 18:11:34 2015 Aurélien Metz
*/

#ifndef CALCUL_H_
# define CALCUL_H_

# include "coord.h"

void	pvector(const t_coord *const, const t_coord *const, t_coord *const);
float	scalaire(const t_coord *const , const t_coord *const);
float	norm(const t_coord *const);
float	dist_seg(t_coord *, t_coord *);

#endif /* !CALCUL_H_ */
