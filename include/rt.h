/*
** rt.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May 24 13:40:49 2015 Aurélien Metz
** Last update Sun May 31 17:51:54 2015 Aurélien Metz
*/

#ifndef RT_H_
# define RT_H_

# include "img.h"

/*
** @mlx_ptr:	Pointeur sur mlx	(mlx)
** @win_ptr:	Pointeur sur fenètre	(mlx)
** @img:	Informations concernant l'image
** @scene	Inorfations concernant la scene
*/
typedef struct	s_rt
{
  void		*mlx_ptr;
  void		*win_ptr;
  void		*scene;
  t_img		img;
}		t_rt;

#endif /* !RT_H_ */
