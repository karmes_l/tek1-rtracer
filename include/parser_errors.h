/*
** parser_errors.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 08:43:00 2015 VIctor Le
** Last update Wed Jun  3 22:55:00 2015 VIctor Le
*/

#ifndef PARSER_ERRORS_H_
# define PARSER_ERRORS_H_

# include "scene.h"
# include "error_managment.h"

enum	e_parser_state
  {
    PARSER_OK = 0,
    PARSER_WARN,
    PARSER_SKIP,
    PARSER_FAIL,
    PARSER_END
  };

# define PARSER_ERR_SCENE	"parser: Internal error with scene\n"
# define PARSER_ERR_FILENAME	"parser: Internal error with filename\n"
# define PARSER_ERR_OPEN	"Cannot open "
# define PARSER_ERR_NO_MATCH	"get_index: No match for "
# define PARSER_ERR_FEW_ARG	"Too few arguments: line "
# define PARSER_ERR_BAD_RAYON	"Invalid rayon\n"
# define PARSER_ERR_BAD_MAT	"Sum of material values is not equal to 100\n"
# define PARSER_ERR_BAD_ANGLE	"Bad angle: Negative or null angle\n"
# define PARSER_ERR_NULL_ROT	"A rotation vector is null\n"
# define PARSER_ERR_FIELD(x)	x": Necessary fields aren't fill or invalid\n"

# define GET_INDEX_ERR_WORD	"get_index: Internal error with word\n"
# define GET_INDEX_ERR_CMP_WORD	"get_index: Internal error with cmp_word\n"

# define PARSER_WARN_MANY_ARG	"Useless arguments: line "
# define PARSER_WARN_NEG_VALUE	"A negative value has been set to 0\n"
# define PARSER_WARN_REDEF_MAT	"Redefining material of object line "

# define PARSER_NO_MATCH	(-1)

void	putwarn(const char *const s);
void	putnbr_warn(const int nb);
void	putnbr_err(const int nb);

int	parser_err_param(t_scene *scene, const char *const filename);
int	parser_err_open(const char *const filename);
int	parser_err_warn(const char *const msg, const int line, const int err);
int	get_index_err_param(const char *const word, char **cmp_word);

#endif /* !PARSER_ERRORS_H_ */
