/*
** fill_fct_tools.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed May 27 13:41:21 2015 VIctor Le
** Last update Thu May 28 21:59:17 2015 VIctor Le
*/

#ifndef FILL_FCT_TOOLS_H_
# define FILL_FCT_TOOLS_H_

# include "coord.h"

char	**fill_fct_set_wdtab(char **s, int *ret, const int fd, int *line);
int	check_nb_field_args(const int min_arg, char **wdtab, int *line);
int	check_null_rotation(t_coord *rotation);

#endif /* !FILL_FCT_TOOLS_H_ */
