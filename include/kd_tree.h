/*
** kd_tree.h for  in /home/karmes_l/Projets/Igraph/raytracer/MUL_2014_rtracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May 28 22:44:24 2015 lionel karmes
** Last update Sun Jun  7 20:27:20 2015 huy le
*/

#ifndef KD_TREE_H_
# define KD_TREE_H_

#include "coord.h"

/*
** Arbre binaire triant les points de la façon kd tree (cf. wikipedia)
** @left:			Pointe vers la branche gauche
** @right:			Pointe vers la branche droite
** @location:			Contient les coordonés d'un points
*/
typedef struct		s_kd_tree
{
  struct s_kd_tree	*left;
  struct s_kd_tree	*right;
  t_coord		location;
}			t_kd_tree;

#endif /* !KD_TREE_H_ */
