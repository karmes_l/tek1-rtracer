/*
** secure_free.h for RT in /home/le_l/rendu/Igraph/MUL_2014_rtracer
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 08:38:34 2015 VIctor Le
** Last update Wed Jun  3 18:39:23 2015 Aurélien Metz
*/

#ifndef SECURE_FREE_H_
# define SECURE_FREE_H_

/*
** Vérifie que le pointeur n'est pas NULL et le free.
** Pas besoin de cast. Segfault toujours possible.
** Retourne NULL.
*/
void	*secure_free(void *ptr);

#endif /* !SECURE_FREE_H_ */
