/*
** obj.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 14:10:18 2015 Aurélien Metz
** Last update Thu Jun  4 18:30:48 2015 Aurélien Metz
*/

#ifndef OBJ_H_
# define OBJ_H_

# include "coord.h"
# include "color.h"
# include "material.h"

enum	e_obj_type
  {
    OT_VOID = 0,
    OT_SPHERE,
    OT_CONE,
    OT_CYLINDER,
    OT_PLAN,
    OT_TRIANGLE,
    OT_PARA,
    OT_DISC
  };

/*
** @pos:		Origin de l'objet
** @rotation:		Vecteurs directeurs de l'objet
**			(champ optionel selon l'objet)
** @broad_cast:		Composante de ré-émission de l'objet
** @transparent:	Composante de transparence de l'objet
** @reflect:		Composante de reflection de l'objet
** @color:		Informations concernants la couleur de l'objet
** @dist:		Distance entre l'oeil et l'objet en unitée de vecteur
** @next:		Pointeur vers le prochain objet
** @rayon:		Rayon d'une sphère et cylindre SEULEMENT
** @angle:		Angle d'ouverture d'un cône SEULEMENT.
*/
typedef struct	s_obj
{
  struct s_obj	*next;
  t_coord	pos;
  t_coord	rotation[2];
  t_color	color;
  t_material	material;
  float		dist;
  float		rayon;
  unsigned int	angle;
  unsigned int	limit[2];
  char		type;
}		t_obj;

/*
** @size:	Indicateur de la taille de la liste
** @head:	Pointeur sur le premier objet
** @tail:	Pointeur sur le dernier objet
*/
typedef struct	s_list_obj
{
  t_obj		*head;
  t_obj		*tail;
  unsigned int	size;
}		t_list_obj;

#endif /* !OBJ_H_ */
