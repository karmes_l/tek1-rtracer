/*
** get_normal.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 15:21:40 2015 Aurélien Metz
** Last update Sun Jun  7 00:03:15 2015 Aurélien Metz
*/

#ifndef GET_NORMAL_H_
# define GET_NORMAL_H_

# include <stdlib.h>
# include "nbr_obj.h"
# include "obj.h"

void	norm_vect_sphere(t_coord *const, const t_obj *const,
			 const t_coord *const);
void	norm_vect_cone(t_coord *const, const t_obj *const,
		       const t_coord *const);
void	norm_vect_cy(t_coord *const, const t_obj *const,
		     const t_coord *const);
void	norm_vect_plan(t_coord *const, const t_obj *const,
		       const t_coord *const);
typedef void	(*t_normal_obj)(t_coord *const, const t_obj *const,
				const t_coord *const);

#endif /* !GET_NORMAL_H_ */
