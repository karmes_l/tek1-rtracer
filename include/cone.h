/*
** cone.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Thu Jun  4 18:36:04 2015 Aurélien Metz
** Last update Sun Jun  7 21:25:05 2015 huy le
*/

#ifndef CONE_H_
# define CONE_H_

# include <math.h>
# include "ray.h"
# include "obj.h"
# include "formules.h"

void	get_impact(t_coord *const, const t_ray *const, const float);
float	scalaire(const t_coord *const, const t_coord *const);
float	norm(const t_coord *const);

#endif /* !CONE_H_ */
