/*
** t_kd_tree.h for  in /home/karmes_l/Projets/Igraph/raytracer/MUL_2014_rtracer/srcs
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu May 28 22:41:06 2015 lionel karmes
** Last update Sun Jun  7 21:21:53 2015 huy le
*/

#ifndef T_KD_TREE_H
# define T_KD_TREE_H

#include "kd_tree.h"

t_kd_tree	*t_kd_tree_constructor();
t_kd_tree	*t_kd_tree_filling(t_coord *, int, int);
void		t_kd_tree_print(t_kd_tree *);
void		t_kd_tree_free(t_kd_tree *);
int		t_kd_tree_range_search(t_kd_tree *, t_coord *, float, int);

#endif /* !T_KD_TREE_H_ */
