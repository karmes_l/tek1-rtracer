/*
** nbr_obj.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 15:49:39 2015 Aurélien Metz
** Last update Mon Jun  1 15:50:11 2015 Aurélien Metz
*/

#ifndef NBR_OBJ_H_
# define NBR_OBJ_H_

# define NBR_OBJ		(4)

#endif /* !NBR_OBJ_H_ */
