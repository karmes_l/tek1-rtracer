/*
** gnl.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 17:24:18 2015 VIctor Le
** Last update Mon May 25 21:50:18 2015 VIctor Le
*/

#ifndef GNL_H_
# define GNL_H_

enum	e_gnl_bool
  {
    GNL_CONTINUE = 0,
    GNL_END
  };

# define GNL_ERR_STRNCAT	("gnl_strncat: dest or src is NULL\n")
# define GNL_ERR_MALLOC		("gnl_realloc: malloc failed\n")
# define GNL_ERR_READ		("gnl: read failed\n")
# define GNL_BUFFER		(100)

char	*gnl(const int fd);

#endif /* !GNL_H_ */
