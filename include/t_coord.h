/*
** t_coord.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:10:41 2015 VIctor Le
** Last update Fri May 29 18:23:48 2015 lionel karmes
*/

#ifndef T_COORD_H_
# define T_COORD_H_

# include "coord.h"

void	t_coord_init(t_coord *coord);
void	t_coord_cpy(t_coord *const, const t_coord *const);
void	t_coord_swap_coord(t_coord *, t_coord *);

#endif /* !T_COORD_H_ */
