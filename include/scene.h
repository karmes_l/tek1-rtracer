/*
** scene.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 13:50:50 2015 Aurélien Metz
** Last update Sun Jun  7 21:29:28 2015 huy le
*/

#ifndef SCENE_H_
# define SCENE_H_

# include "eye.h"
# include "obj.h"
# include "light.h"

/*
** @eye:	Information concernant loeil
** @map:	map de photon
** @list_obj	liste d'objet
** @list_light	list de spot lumineux
*/
typedef struct	s_scene
{
  t_eye		eye;
  t_list_obj	list_obj;
  t_list_light	list_light;
}		t_scene;

#endif /* !SCENE_H_ */
