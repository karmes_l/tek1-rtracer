/*
** sphere.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 16:34:03 2015 Aurélien Metz
** Last update Mon Jun  1 16:35:59 2015 Aurélien Metz
*/

#ifndef SPHERE_H_
# define SPHERE_H_

#include <math.h>
#include "formules.h"
#include "obj.h"
#include "ray.h"

float	norm(const t_coord *const u);

#endif /* !SPHERE_H_ */
