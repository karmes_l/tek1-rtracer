/*
** coord.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:11:38 2015 VIctor Le
** Last update Mon May 25 16:12:24 2015 VIctor Le
*/

#ifndef COORD_H_
# define COORD_H_

typedef struct	s_coord
{
  float		x;
  float		y;
  float		z;
}		t_coord;

#endif /* !COORD_H_ */
