/*
** field.h for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 30 16:06:24 2015 VIctor Le
** Last update Wed Jun  3 12:16:50 2015 VIctor Le
*/

#ifndef FIELD_H_
# define FIELD_H_

enum
  {
    F_VOID = 0,
    F_POS = 1,
    F_LOOK = 2,
    F_INTST = 4,
    F_MATERIAL = 8,
    F_COLOR = 16,
    F_RAYON = 32,
    F_ANGLE = 64,
    F_ROTATION = 128,
    F_ABS = 256,
    F_LIM = 512
  };

# define NEC_FIELD_SP	(F_RAYON)
# define NEC_FIELD_CO	(F_ANGLE | F_ROTATION)
# define NEC_FIELD_CY	(F_RAYON | F_ROTATION)
# define NEC_FIELD_PL	(F_POS)
# define NEC_FIELD_TR	(F_ROTATION)
# define NEC_FIELD_PA	(F_ROTATION)
# define NEC_FIELD_DI	(F_RAYON | F_ROTATION)

# define MAT_FIELD_BC	("broadcast")
# define MAT_FIELD_TR	("transmit")
# define MAT_FIELD_RE	("reflect")
# define MAT_NB_FIELD	(3)

void	init_field_tab(int *f_value);

#endif /* !FIELD_H_ */
