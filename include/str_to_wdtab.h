/*
** str_to_wdtab.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 22:22:03 2015 VIctor Le
** Last update Tue May 26 23:16:40 2015 VIctor Le
*/

#ifndef STR_TO_WDTAB_H_
# define STR_TO_WDTAB_H_

enum	e_stw_bool
  {
    STW_OK = 0,
    STW_FAIL
  };

# define STW_ERR_NULL_PARAM	("str_to_wdtab: s or delim is NULL.\n")
# define STW_ERR_MALLOC		("str_to_wdtab: malloc failed.\n")

char	**str_to_wdtab(const char *s, const char *const delim);

#endif /* !STR_TO_WDTAB_H_ */
