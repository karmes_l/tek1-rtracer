/*
** fill_obj_fct.h for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed Jun  3 21:52:53 2015 VIctor Le
** Last update Wed Jun  3 22:41:00 2015 VIctor Le
*/

#ifndef FILL_OBJ_FCT_H_
# define FILL_OBJ_FCT_H_

# include "obj.h"

typedef int	(*t_obj_fct)(t_obj *, char **, int *);

int	fill_obj_set_pos(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_angle(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_rotation(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_material(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_color(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_rayon(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_abs(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_lim(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_bi_lim(t_obj *obj, char **wdtab, int *line);
int	fill_obj_set_bi_rotation(t_obj *obj, char **wdtab, int *line);

#endif /* !FILL_OBJ_FCT_H_ */
