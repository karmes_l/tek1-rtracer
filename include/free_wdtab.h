/*
** free_wdtab.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 22:56:55 2015 VIctor Le
** Last update Wed May 27 17:06:23 2015 VIctor Le
*/

#ifndef FREE_WDTAB_H_
# define FREE_WDTAB_H_

void	*free_wdtab(char **wdtab);

#endif /* !FREE_WDTAB_H_ */
