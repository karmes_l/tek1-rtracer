/*
** parser.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:54:49 2015 VIctor Le
** Last update Sun Jun  7 16:15:02 2015 VIctor Le
*/

#ifndef PARSER_H_
# define PARSER_H_

# include <sys/types.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include "scene.h"
# include "gnl.h"
# include "str_to_wdtab.h"
# include "my_strcasecmp.h"
# include "free_wdtab.h"
# include "parser_errors.h"
# include "secure_free.h"
# include "parser_filler_fct.h"
# include "parser_tools.h"

# define PARSER_FIELD_POS	"position"
# define PARSER_FIELD_LOOK	"look"
# define PARSER_FIELD_INTST	"intensity"
# define PARSER_FIELD_MATERIAL	"material"
# define PARSER_FIELD_COLOR	"color"
# define PARSER_FIELD_RAYON	"rayon"
# define PARSER_FIELD_ANGLE	"angle"
# define PARSER_FIELD_ROTATION	"rotation"
# define PARSER_FIELD_ABS	"absorption"
# define PARSER_FIELD_LIM	"limit"

# define PARSER_NB_ARG_COORD	(4)
# define PARSER_NB_ARG_POS	(4)
# define PARSER_NB_ARG_INTST	(2)
# define PARSER_NB_ARG_MATERIAL	(2)
# define PARSER_NB_ARG_COLOR	(4)
# define PARSER_NB_ARG_RAYON	(2)
# define PARSER_NB_ARG_ANGLE	(2)
# define PARSER_NB_ARG_ROTATION	(4)
# define PARSER_NB_ARG_BI_ROT	(7)
# define PARSER_NB_ARG_ABS	(2)
# define PARSER_NB_ARG_BI_LIM	(3)
# define PARSER_NB_ARG_LIM	(2)

# define PARSER_EYE_FIELDS	(2)
# define PARSER_LIGHT_FIELDS	(2)
# define PARSER_SP_FIELDS	(5)
# define PARSER_CO_FIELDS	(7)
# define PARSER_CY_FIELDS	(7)
# define PARSER_PL_FIELDS	(5)
# define PARSER_TR_FIELDS	(5)
# define PARSER_PA_FIELDS	(5)
# define PARSER_DI_FIELDS	(6)

# define PARSER_KW_EYE		"eye"
# define PARSER_KW_LIGHT	"light"
# define PARSER_KW_SPHERE	"sphere"
# define PARSER_KW_CONE		"cone"
# define PARSER_KW_CYLINDER	"cylinder"
# define PARSER_KW_PLAN		"plan"
# define PARSER_KW_TRIANGLE	"triangle"
# define PARSER_KW_PARA		"parallelepiped"
# define PARSER_KW_DISC		"disc"
# define PARSER_NB_KW		(9)

typedef int	(*t_fill)(t_scene *, const int, int *, char **);

int	parser(t_scene *scene, const char *const filename);

#endif /* !PARSER_H_ */
