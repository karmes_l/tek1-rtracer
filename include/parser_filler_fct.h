/*
** parser_filler_fct.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 26 15:52:56 2015 VIctor Le
** Last update Mon Jun  1 18:30:16 2015 VIctor Le
*/

#ifndef PARSER_FILLER_FCT_H_
# define PARSER_FILLER_FCT_H_

# include "scene.h"

int	fill_eye(t_scene *scene, const int fd, int *line, char **s);
int	fill_light(t_scene *scene, const int fd, int *line, char **s);
int	fill_sphere(t_scene *scene, const int fd, int *line, char **s);
int	fill_cone(t_scene *scene, const int fd, int *line, char **s);
int	fill_cylinder(t_scene *scene, const int fd, int *line, char **s);
int	fill_plan(t_scene *scene, const int fd, int *line, char **s);
int	fill_triangle(t_scene *scene, const int fd, int *line, char **s);
int	fill_para(t_scene *scene, const int fd, int *line, char **s);
int	fill_disc(t_scene *scene, const int fd, int *line, char **s);

#endif /* !PARSER_FILLER_FCT_H_ */
