/*
** error_managment.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 16:14:14 2015 Aurélien Metz
** Last update Tue May 26 00:02:27 2015 VIctor Le
*/

#ifndef ERROR_MANAGMENT_H_
# define ERROR_MANAGMENT_H_

# include <stdlib.h>

# define USAGE		("--- USAGE ---\n./rt [FILE]\n")
# define NULL_ENV	("RayTracer can't work with empty environnement\n")

void	puterror(const char *const);

#endif /* !ERROR_MANAGMENT_H_ */
