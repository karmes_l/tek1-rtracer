/*
** ray.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer/include
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Mon Jun  1 14:09:54 2015 Aurélien Metz
** Last update Thu Jun  4 18:30:24 2015 Aurélien Metz
*/

#ifndef RAY_H_
# define RAY_H_

# include "coord.h"

typedef struct	s_ray
{
  t_coord	pos[2];
  float		intensity;
}		t_ray;

#endif /* !RAY_H_ */
