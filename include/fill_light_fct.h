/*
** fill_light_fct.h for rt in /home/le_l/workspace/rt/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 28 15:35:33 2015 VIctor Le
** Last update Thu May 28 15:36:20 2015 VIctor Le
*/

#ifndef FILL_LIGHT_FCT_H_
# define FILL_LIGHT_FCT_H_

typedef int	(*t_light_fct)(t_light *, char **, int *);

int	fill_light_set_pos(t_light *light, char **wdtab, int *line);
int	fill_light_set_intensity(t_light *light, char **wdtab, int *line);

#endif /* !FILL_LIGHT_FCT_H_ */
