/*
** main.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 16:00:49 2015 Aurélien Metz
** Last update Wed Jun  3 18:45:13 2015 Aurélien Metz
*/

#ifndef MAIN_H_
# define MAIN_H_

# include "rt.h"
# include "scene.h"
# include "mlx.h"

void	draw(t_rt *const, t_scene *const);
int	t_scene_init(t_scene *const, const char *const);
int	t_rt_init(t_rt *const, void *const);
int	error_managment(const int, const char *const *const);
int	expose(const t_rt *const);
int	key(const int);

#endif /* !MAIN_H_ */
