/*
** t_eye.h for rt in /home/le_l/rendu/Igraph/MUL_2014_rtracer/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May 25 16:04:20 2015 VIctor Le
** Last update Thu Jun  4 20:20:43 2015 Aurélien Metz
*/

#ifndef T_EYE_H_
# define T_EYE_H_

# include <math.h>
# include "eye.h"
# include "formules.h"

void	t_coord_init(t_coord *const);

#endif /* !T_EYE_H_ */
