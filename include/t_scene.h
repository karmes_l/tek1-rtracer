/*
** t_scene.h for rt in /home/metz_a/rendu/Graphics programing/MUL_2014_rtracer
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sun May 24 17:36:25 2015 Aurélien Metz
** Last update Sun Jun  7 20:24:59 2015 huy le
*/

#ifndef T_SCENE_H_
# define T_SCENE_H_

# include "scene.h"

# define T_LIGHT_ERR_PARAM	("t_list_light fcts: NULL parameters given\n")
# define T_LIGHT_ERR_MALLOC	("t_list_light fcts: malloc failed\n")
# define T_OBJ_ERR_PARAM	("t_obj fcts: NULL parameters given\n")
# define T_OBJ_ERR_MALLOC	("t_list_light fcts: malloc failed\n")

void	t_list_obj_init(t_list_obj *const);
int	t_list_obj_append(t_list_obj *);
void	t_list_obj_destruct(t_list_obj *const);

void	t_list_light_init(t_list_light *const);
int	t_list_light_append(t_list_light *);
void	t_list_light_destruct(t_list_light *const);

void	t_eye_init(t_eye *const);
void	t_eye_redef_rotation(t_eye *const);

void	t_scene_destruct(t_scene *const);

#endif /* !T_SCENE_H_ */
