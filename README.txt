=== Ray Tracer ===

This guide explain how to use our ray tracer.
It's working with file, so read carefully how to describe your scene
in a file. If you don't follow the indications given in this guide,
the scene might not look as you imagined it.

--- Point of view ---

You can move the point of you using the feild " Eye "
It make you able to change the coordonates of the camera as follow :

   	Position X Y Z

You also can change the angle of the camera :

    	Look [Rotation Z] [Rotation Y] 0

Example:

	Eye
	Position 300 0 100
	Look 0 -10 0

If you don't precise any of this sub-feild or even the feild " Eye "
the Default values are

    	Position 0 0 0
	Look 0 0 0
---------------------


--- Lighting ---

You can manage the lighting adding lights using feild " Light "
You can precise their coordinate as follow :

    	Position X Y Z

You also can choose their power of lighting:

    	Intensity [value]

(any color wich has recieved a light of intensity 100 is seen as described)
Example:

	Light
	Position 4000 0 500
	Intensity 200

If you don't precise the sub-feild Position
the Default values are

    	Position 0 0 0

Other feilds are mandatory for every lights
----------------


--- Contained ---

You can describe the scene adding objects using feilds " Sphere, Cone, Cylinder or Plan "
You can precise the coordinates of every objects as follow :

    	Position X Y Z

You also can precise the color of every objects as follow :

    	Color red gree blue

Some objects need to be rotate, for that you will write the guiding vector
Be carefull, for the Plan you must describe two guiding vectors in a way
that the vectorial product point you're eye's side.

     	Rotation X Y Z (X2 Y2 Z2)

You can finaly for any objects add an absorption composant (limiting the light difused)

    	Absorption [value(% )]

(the value of absorption must not exceed 100)

Examples:

	Sphere
	Position X Y Z		(Optional)
	Rayon [value]		(Mandatory)
	Color red green blue	(Optional)

	Cone
	Position X Y Z		(Optional)
	Angle [value]		(Mandatory)
	Rotation X Y Z		(Mandatory)
	Color red green blue	(Optional)

	Cylinder
	Position X Y Z		(Optional)
	Rayon [value]		(Mandatory)
	Rotation X Y Z		(Mandatory)
	Color red green blue	(Optional)

	Plan
	Position X Y Z		(Optional)
	Rotation X Y Z X2 Y2 Z2	(Mandatory)
	Color red green blue	(Optional)

-----------------
